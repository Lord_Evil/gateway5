#!/bin/bash
function save(){
        FILES=`find . -not \( -path ./.git  -prune \) -not \( -path ./include -prune \) -not \( -path ./.tmp -prune \) -not \( -path ./assets -prune \) -type f \( -iname "*" ! -iname "Manifest.md5" ! -iname "*.pyc" ! -iname "*.o" \) -exec bash -c 'echo "$0"' {} \;`
	md5sum $FILES 2>/dev/null >Manifest.md5
}
function check(){
	FILES=`find . -not \( -path ./.git  -prune \) -not \( -path ./include -prune \) -not \( -path ./.tmp -prune \) -not \( -path ./assets -prune \) -type f \( -iname "*" ! -iname "Manifest.md5" ! -iname "*.pyc" ! -iname "*.o" \) -exec bash -c 'echo "$0"' {} \;`
	md5sum $FILES 2>/dev/null >.tmp/Manifest.md5
        diff Manifest.md5 .tmp/Manifest.md5
}

while getopts ":sch" opt; do
  case $opt in
    s)
      echo "Saving..."
      save
      ;;
    c)
      echo "Checking..."
      check
      ;;
    h)
      echo "Usage: ./version <command>"
      echo "-s - Save current chechsums for all files"
      echo "-c - Compare saved checksums with current"
      echo "-h - Show this message"
    ;;
  esac
done
