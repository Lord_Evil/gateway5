//var argv = require( 'argv' );
//var args = argv.option( {name:"dump",type:"path"} ).run();
//console.log(args["options"]["dump"])
//console.log(argv["name"])

var readline = require('readline');
var fs = require('fs');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var config = JSON.parse(fs.readFileSync("config.json").toString());
var database=config["modules"]["db"]["database"];
var url="mongodb://localhost:27017/"+database;

var dumps=fs.readdirSync("dumps");
var ki=dumps.indexOf(".keep")
if(ki>-1){
	dumps.shift(0);
}
if(dumps.length>0){
	selectDump();
}
function listDumps(){
	list="";
	list+="Which one of the following dumps would you like to import?\n";
	var i=1;
	dumps.forEach(function(val){
		list+=i+")"+val+"\n";
		i++;
	});
	return list;
}
function selectDump(){
	var rl = readline.createInterface({input:process.stdin, output:process.stdout});
	rl.question(listDumps(), function(n) {
		if(n-1<dumps.length&&n-1>-1){
			console.log("Ok, importing dump: ", dumps[n-1]);
			readDump(dumps[n-1]);
			rl.close();
		}else{
			rl.close();
			selectDump();
		}
	});
}
function readDump(file){
	var file = fs.readFileSync("dumps/"+file).toString();
	var dump=JSON.parse(file);
	importDump(dump);
}
function importDump(dump){
	MongoClient.connect(url, function(err, db) {
		assert.equal(null, err);
		console.log("Connected correctly to MongoDB server");
		db.dropDatabase();
		db.close();
		console.log("Database '"+database+"' cleared!");
		Object.keys(dump).forEach(function(key,val){
			MongoClient.connect(url, function(err, db) {
				assert.equal(null, err);
				var collection=db.collection(key);
				collection.drop();
				collection.insert(dump[key],function(err,res){
					console.log("Inserted "+res.ops.length.toString()+" documents into collection '"+key+"'");
					db.close();
				});
			});
		});
	});
}