conn = new Mongo();
conf=JSON.parse(cat("config.json"));
db = conn.getDB(conf["modules"]["db"]["database"]);
var a=db.system.indexes.find({},{"ns":true});
names=[];
a.forEach(function(val){names.push((val["ns"].substr(val["ns"].indexOf(".")+1)))});
collections={};
for(i=0;i<names.length;i++){
	var tb = db[names[i]].find({},{"_id":false});
	collections[names[i]]=[];
	tb.forEach(function(val){
		collections[names[i]].push(val);
	})
}
print(JSON.stringify(collections));

