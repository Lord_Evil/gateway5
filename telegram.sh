#!/bin/bash
cd "`dirname "$0"`"
pwd
export LD_LIBRARY_PATH=./
rm ./assets/telegram/*.sock
export TELEGRAM_HOME=./assets/telegram
export TELEGRAM_CONFIG_DIR=./assets/telegram/config
./bin/telegram-cli -W
