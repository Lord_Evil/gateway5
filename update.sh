#!/bin/bash
git pull --recurse-submodules
git submodule foreach -q --recursive 'branch="$(git config -f $toplevel/.gitmodules submodule.$name.branch)"; git pull origin $branch; git checkout $branch'
make -C src/modules/web/ install-assets

