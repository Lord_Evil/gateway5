import core.stdc.config;

extern (C):
/* namespace defines */
 enum {
/** @def XMPP_NS_CLIENT
 *  Namespace definition for 'jabber:client'.
 */
    NS_CLIENT = "jabber:client",
/** @def XMPP_NS_COMPONENT
 *  Namespace definition for 'jabber:component:accept'.
 */
    NS_COMPONENT = "jabber:component:accept",
/** @def XMPP_NS_STREAMS
 *  Namespace definition for 'http://etherx.jabber.org/streams'.
 */
    NS_STREAMS = "http://etherx.jabber.org/streams",
/** @def XMPP_NS_STREAMS_IETF
 *  Namespace definition for 'urn:ietf:params:xml:ns:xmpp-streams'.
 */
    NS_STREAMS_IETF = "urn:ietf:params:xml:ns:xmpp-streams",
/** @def XMPP_NS_TLS
 *  Namespace definition for 'url:ietf:params:xml:ns:xmpp-tls'.
 */
    NS_TLS = "urn:ietf:params:xml:ns:xmpp-tls",
/** @def XMPP_NS_SASL
 *  Namespace definition for 'urn:ietf:params:xml:ns:xmpp-sasl'.
 */
    NS_SASL = "urn:ietf:params:xml:ns:xmpp-sasl",
/** @def XMPP_NS_BIND
 *  Namespace definition for 'urn:ietf:params:xml:ns:xmpp-bind'.
 */
    NS_BIND = "urn:ietf:params:xml:ns:xmpp-bind",
/** @def XMPP_NS_SESSION
 *  Namespace definition for 'urn:ietf:params:xml:ns:xmpp-session'.
 */
    NS_SESSION = "urn:ietf:params:xml:ns:xmpp-session",
/** @def XMPP_NS_AUTH
 *  Namespace definition for 'jabber:iq:auth'.
 */
    NS_AUTH = "jabber:iq:auth",
/** @def XMPP_NS_DISCO_INFO
 *  Namespace definition for 'http://jabber.org/protocol/disco#info'.
 */
    NS_DISCO_INFO = "http://jabber.org/protocol/disco#info",
/** @def XMPP_NS_DISCO_ITEMS
 *  Namespace definition for 'http://jabber.org/protocol/disco#items'.
 */
    NS_DISCO_ITEMS = "http://jabber.org/protocol/disco#items",
/** @def XMPP_NS_ROSTER
 *  Namespace definition for 'jabber:iq:roster'.
 */
    NS_DISCO_CHAT = "http://jabber.org/protocol/chatstates",

    NS_ROSTER = "jabber:iq:roster"
}
/* error defines */
enum {
/** @def XMPP_EOK
 *  Success error code.
 */
    ERR_EOK = 0,
/** @def XMPP_EMEM
 *  Memory related failure error code.
 *  
 *  This is returned on allocation errors and signals that the host may
 *  be out of memory.
 */
    ERR_EMEM = -1,
/** @def XMPP_EINVOP
 *  Invalid operation error code.
 *
 *  This error code is returned when the operation was invalid and signals
 *  that the Strophe API is being used incorrectly.
 */
    ERR_EINVOP = -2,
/** @def XMPP_EINT
 *  Internal failure error code.
 */
    ERR_EINT = -3
}
alias _xmpp_mem_t xmpp_mem_t;
alias _xmpp_log_t xmpp_log_t;
alias _xmpp_ctx_t xmpp_ctx_t;
alias void function (void*, xmpp_log_level_t, const(char)*, const(char)*) xmpp_log_handler;
alias _xmpp_conn_t xmpp_conn_t;
alias _xmpp_stanza_t xmpp_stanza_t;
alias extern (C) void function (_xmpp_conn_t*, xmpp_conn_event_t, int, xmpp_stream_error_t*, void*) xmpp_conn_handler;
alias int function (_xmpp_conn_t*, void*) xmpp_timed_handler;
alias int function (_xmpp_conn_t*, _xmpp_stanza_t*, void*) xmpp_handler;

enum xmpp_log_level_t
{
    DEBUG = 0,
    INFO = 1,
    WARN = 2,
    ERROR = 3
}

enum xmpp_conn_type_t
{
    XMPP_UNKNOWN = 0,
    XMPP_CLIENT = 1,
    XMPP_COMPONENT = 2
}

enum xmpp_conn_event_t
{
    CONNECT = 0,
    DISCONNECT = 1,
    FAIL = 2
}

enum xmpp_error_type_t
{
    BAD_FORMAT = 0,
    BAD_NS_PREFIX = 1,
    CONFLICT = 2,
    CONN_TIMEOUT = 3,
    HOST_GONE = 4,
    HOST_UNKNOWN = 5,
    IMPROPER_ADDR = 6,
    INTERNAL_SERVER_ERROR = 7,
    INVALID_FROM = 8,
    INVALID_ID = 9,
    INVALID_NS = 10,
    INVALID_XML = 11,
    NOT_AUTHORIZED = 12,
    POLICY_VIOLATION = 13,
    REMOTE_CONN_FAILED = 14,
    RESOURCE_CONSTRAINT = 15,
    RESTRICTED_XML = 16,
    SEE_OTHER_HOST = 17,
    SYSTEM_SHUTDOWN = 18,
    UNDEFINED_CONDITION = 19,
    UNSUPPORTED_ENCODING = 20,
    UNSUPPORTED_STANZA_TYPE = 21,
    UNSUPPORTED_VERSION = 22,
    XML_NOT_WELL_FORMED = 23
}

struct _xmpp_mem_t
{
    void* function (const size_t, void*) alloc;
    void function (void*, void*) free;
    void* function (void*, const size_t, void*) realloc;
    void* userdata;
}

struct _xmpp_log_t
{
    xmpp_log_handler handler;
    void* userdata;
}

struct xmpp_stream_error_t
{
    xmpp_error_type_t type;
    char* text;
    xmpp_stanza_t* stanza;
}

struct _xmpp_conn_t;


struct _xmpp_ctx_t;


struct _xmpp_stanza_t;


void xmpp_initialize ();
void xmpp_shutdown ();
int xmpp_version_check (int major, int minor);
xmpp_ctx_t* xmpp_ctx_new (const xmpp_mem_t* mem, const xmpp_log_t* log);
void xmpp_ctx_free (xmpp_ctx_t* ctx);
xmpp_log_t* xmpp_get_default_logger (xmpp_log_level_t level);
xmpp_conn_t* xmpp_conn_new (xmpp_ctx_t* ctx);
xmpp_conn_t* xmpp_conn_clone (xmpp_conn_t* conn);
int xmpp_conn_release (xmpp_conn_t* conn);
const(char)* xmpp_conn_get_jid (const xmpp_conn_t* conn);
const(char)* xmpp_conn_get_bound_jid (const xmpp_conn_t* conn);
void xmpp_conn_set_jid (xmpp_conn_t* conn, const char* jid);
const(char)* xmpp_conn_get_pass (const xmpp_conn_t* conn);
void xmpp_conn_set_pass (xmpp_conn_t* conn, const char* pass);
xmpp_ctx_t* xmpp_conn_get_context (xmpp_conn_t* conn);
void xmpp_conn_disable_tls (xmpp_conn_t* conn);
int xmpp_connect_client (xmpp_conn_t* conn, const char* altdomain, ushort altport, xmpp_conn_handler callback, void* userdata);
int xmpp_connect_component (xmpp_conn_t* conn, const char* server, ushort port, xmpp_conn_handler callback, void* userdata);
void xmpp_disconnect (xmpp_conn_t* conn);
void xmpp_send (xmpp_conn_t* conn, xmpp_stanza_t* stanza);
void xmpp_send_raw_string (xmpp_conn_t* conn, const char* fmt, ...);
void xmpp_send_raw (xmpp_conn_t* conn, const char* data, const size_t len);
void xmpp_timed_handler_add (xmpp_conn_t* conn, xmpp_timed_handler handler, const c_ulong period, void* userdata);
void xmpp_timed_handler_delete (xmpp_conn_t* conn, xmpp_timed_handler handler);
void xmpp_handler_add (xmpp_conn_t* conn, xmpp_handler handler, const char* ns, const char* name, const char* type, void* userdata);
void xmpp_handler_delete (xmpp_conn_t* conn, xmpp_handler handler);
void xmpp_id_handler_add (xmpp_conn_t* conn, xmpp_handler handler, const char* id, void* userdata);
void xmpp_id_handler_delete (xmpp_conn_t* conn, xmpp_handler handler, const char* id);
xmpp_stanza_t* xmpp_stanza_new (xmpp_ctx_t* ctx);
xmpp_stanza_t* xmpp_stanza_clone (xmpp_stanza_t* stanza);
xmpp_stanza_t* xmpp_stanza_copy (const xmpp_stanza_t* stanza);
int xmpp_stanza_release (xmpp_stanza_t* stanza);
void xmpp_free (const xmpp_ctx_t* ctx, void* p);
int xmpp_stanza_is_text (xmpp_stanza_t* stanza);
int xmpp_stanza_is_tag (xmpp_stanza_t* stanza);
int xmpp_stanza_to_text (xmpp_stanza_t* stanza, char** buf, size_t* buflen);
xmpp_stanza_t* xmpp_stanza_get_children (xmpp_stanza_t* stanza);
xmpp_stanza_t* xmpp_stanza_get_child_by_name (xmpp_stanza_t* stanza, const char* name);
xmpp_stanza_t* xmpp_stanza_get_child_by_ns (xmpp_stanza_t* stanza, const char* ns);
xmpp_stanza_t* xmpp_stanza_get_next (xmpp_stanza_t* stanza);
char* xmpp_stanza_get_attribute (xmpp_stanza_t* stanza, const char* name);
char* xmpp_stanza_get_ns (xmpp_stanza_t* stanza);
char* xmpp_stanza_get_text (xmpp_stanza_t* stanza);
char* xmpp_stanza_get_text_ptr (xmpp_stanza_t* stanza);
char* xmpp_stanza_get_name (xmpp_stanza_t* stanza);
int xmpp_stanza_add_child (xmpp_stanza_t* stanza, xmpp_stanza_t* child);
int xmpp_stanza_set_ns (xmpp_stanza_t* stanza, const char* ns);
int xmpp_stanza_set_attribute (xmpp_stanza_t* stanza, const char* key, const char* value);
int xmpp_stanza_set_name (xmpp_stanza_t* stanza, const char* name);
int xmpp_stanza_set_text (xmpp_stanza_t* stanza, const char* text);
int xmpp_stanza_set_text_with_size (xmpp_stanza_t* stanza, const char* text, const size_t size);
char* xmpp_stanza_get_type (xmpp_stanza_t* stanza);
char* xmpp_stanza_get_id (xmpp_stanza_t* stanza);
int xmpp_stanza_set_id (xmpp_stanza_t* stanza, const char* id);
int xmpp_stanza_set_type (xmpp_stanza_t* stanza, const char* type);
void xmpp_run_once (xmpp_ctx_t* ctx, const c_ulong timeout);
void xmpp_run (xmpp_ctx_t* ctx);
void xmpp_stop (xmpp_ctx_t* ctx);