#!/bin/bash
function list(){
	for line in `screen -list|grep -oP "(\d+).([a-z]+)\t"|grep -oP "([a-z]+)"`
		do 
			if [ $line = $1 ]
				then 
				echo "running"
				return 1
			fi
	done
	echo "stopped"
}
function startGateway(){
	echo "###########################"
	date
	echo -n Starting Gateway...
	screen -S cc -d -m sh -c "./gateway.sh >> gw-log.log"
	sleep 1
	echo Done.
}
function checkService(){
	mc=$(list "mc")
	cc=$(list "cc")
	if [ $mc == "stopped" ]
		then
		startMongo
	fi
	if [ $cc == "stopped" ]
		then
		startGateway
	fi
}

while true
do
	checkService
	sleep 1
done
