Gateway5 - a message bus, which serves publishers and subscribers (modules with C interface), augmented with HTTP server, with generates separate type of event. 

Module can set up a route for specific HTTP paths (e.g. /auth, /ws, /modules/function/param, like /<mod_name>/<path> etc). These routes are loaded on Gateway5 start up. All HTTP requests that match this pattern will be forwarded to a module's handler function using blocking function call (client will wait for HTTP response).

HTTP server support web-sockets.

Modules can subscribe for any event and generate events, so they are communicating with each other.

** Think about: ** Replace or augment it with RabbitMQ

# Modules #

Currently there are 7 modules:

## Auth 

Serves a static HTML page (from /auth/) for login, implement login / logout using messaging services and creates session on successful user login. Based on cookie.

## API

URL: /api/json

Requires authorization via Auth (if authentication is required in config)

Converts HTTP JSON calls into internal bus messages (EVENTS only), and waits for a response from each subscriber (if any). Calls are blocking.

CoreZoid can call API being authorized with its signature, not cookie.

## BotZoid 

CoreZoid related - Telegram messages are sent to CoreZoid, can be used to talk to CoreZoid. NOT USED NOW.

## Conv

Timer. NOT USED NOW

## DB

No autherntication required, so must be run locally
Used by Mongo Client. Used at separate copy of Gateway, which serves only MongoDB access

## Hangouts

Subscribes /hangouts/push, /hangouts/save - internal functions, unprotected :-(

NO AUTHENTICATION. ** TODO: ** One-time key authentication

Communication with Google Hangouts account via XMPP. User name and password in the Gateway5 config file.

## Manager

Subscribes to /manager/sendMessage without authentication and delivers messages to whoever with either ChatneyID to Telegram (ToDo: preferred messenger) or Social+ID.

Also manager provides timer communication functions.

**POST http://arch:6080/manager/setEvent**

```
{
   "data": {
      "text": "Hello",
      "user": {
         "chatneyID": "11094685-7cab-4c74-9da0-7d4906bb4f60"
      }
   },
   "finish": 1452142099
}
```

As a result you will recive "idKey". Keys data.user.chatneyID is a MUST so that manager module will know which user to notify.

```
{"idKey":"5f88babe-44c1-432f-821c-e841d988d5e8","status":"success"}

```
or
```
{
"message": "Finish time in past!"
"status": "fail"
}
```
**POST http://arch:6080/manager/findEvent**
```
{
   "query":{"data.text":"Hello2"}
}
```
where query any MongoDB find query. If query is not set then all results will be displayed
```
{
    "calendars": [
        {
            "callerKey": "dev",
            "idKey": "fe2f0264-38c5-430a-816c-cc8c35bdc367",
            "start": 1452209913,
            "callback": "http://localhost:6080/manager/event/fire",
            "data": {
                "text": "Hello2",
                "user": {
                    "chatneyID": "11094685-7cab-4c74-9da0-7d4906bb4f60"
                }
            },
            "finish": 1454142099
        }
    ],
    "status": "success"
}
```
**POST http://arch:6080/manager/cancelEvent**
```
{
    "idKey": "5f88babe-44c1-432f-821c-e841d988d5e8"
}
```
on sucessful delete 
```
{"message":"event deleted","status":"success"}
```
or
```
{"message":"the event don't exist","status":"success"}
```

## NetMod

Connect remote application as modules (push event / subscribe only, not HTTP request).

Authentication: name/key pair, stored in MongoDB (collection: netmods). **TODO: replace to normal SSL client/server authentication**

## PyBot

Views for MongoDB. Interacts with MongoClient to read/write data from/to MongoDB.

Some obsolete functions: polls, mail lists...

## Telegram

NO AUTHENTICATION. ** TODO: ** One-time key authentication

For pattern /telegram/file/:msgID/:type/:fileName - **TODO**: authenticate using Auth

## Twilio

NO AUTHENTICATION. ** TODO: ** One-time key authentication

/sms/smsin - external route, unprotected :-(

## Web

Serves static files from /web/ , supports web-sockets (with Auth, if ON in config).

Subscribes to _PUSH_MESSAGE_ and _BOTCORE_MESSAGE_ to translate them to all connected Web-sockets.

## Social Template ##

Not for practical use, just for reference for creation new messenger module.

Template to create messenger module, keeps all interfaces.

# MongoDB

Stores

* Contact Lists for all messengers
* Administrators
* NetMod authentication keys
* Chat facilitacion: polls, reminders etc

This will serve as a basis for further robots development

=================
```
sudo apt-get install libreadline-dev python3-apt libconfig-dev libevent-dev xdg-utils
wget http://downloads.dlang.org/releases/2.x/2.068.2/dmd_2.068.2-0_amd64.deb
sudo dpkg -i dmd_2.068.2-0_amd64.deb
sudo apt-get install python3 python3-setuptools python3-bs4 python3-pip
sudo apt-get install --reinstall libpython3.4-stdlib
sudo pip install requests
sudo pip install --upgrade requests
```