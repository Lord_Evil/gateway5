#!/bin/bash
echo == Stopping everything ==
echo -n StoppingMonitor
screen -S mon -p 0 -X stuff $'\cC'

echo -n Stopping Gateway...
screen -S cc -p 0 -X stuff $'\cC'
echo Done.
echo -n Stopping Mongo Client...
screen -S mc -p 0 -X stuff $'\cC'
echo Done.

