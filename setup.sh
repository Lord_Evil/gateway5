#!/bin/bash
PROOT=`pwd`
TGROOT=${PROOT}/src/3rdparty/tg
function mongoSetup {
	m=`ps -e|grep -o mongod`
	if test $m != "mongod"
	then
		echo -e "MongoDB is not running!\nPlease check if it's installed and started! And then try again!"
	else
		if [ ! -e "mongo/bin/gateway-mongo" ]
		then
			mongo mongo/db.js
			echo "Keys for notif and passmail inserted into db"
			make mongo-client
		else
			echo -n "Mongo client is already built. Re-build? [y/n] "
			read want
			if [ "$want" == "y" ]
			then
				mongo mongo/db.js
				echo "Keys for notif and passmail inserted into db"
				make mongo-client
			fi
		fi
                if [ ! -e "mongo/config.json" ]
		then
			cp mongo/config.json.default mongo/config.json
		else
			echo "mongo/config.json was not overwritten!"
		fi
	fi
}
function buildTelegram {
    cd ${TGROOT}
    git reset --hard
    ./configure --disable-liblua --disable-json
    patch -Np1 < ../patch/tg.patch
    make
    strip -s bin/telegram-cli
    git reset --hard
    cd ${PROOT}/bin
    if [ ! -L "${PROOT}/bin/telegram-cli" ]
    then
      ln -s ${TGROOT}/bin/telegram-cli
    fi
    cd ..
}
if [ ! -e ./bin/gateway ]
then
  make app
else
  echo -n "Main App Gateway is already built. Re-build? [y/n] "
  read want
  if [ "$want" == "y" ]
  then
    make app
  fi
fi
mongoSetup
if [ ! -e "./config.json" ]
then
  cp config.json.default config.json
else
  echo "config.json was not overwritten!"
fi
if [ `ls modules/|wc -l` -gt 0 ]
then
  echo "These modules were already built."
  echo -e "\x1b[32m"`ls modules/`"\x1b[0m"
  echo -n "Would you like to rebuild all modules? [y/n] "
  read want
  if [ "$want" == "y" ]
  then
    make modules
  fi
else
   make modules
fi

if [ -s "${PROOT}/bin/telegram-cli" ]
  then
  echo -n "Telegram has been built already. Do you want to re-build? [y/n] "
  read want
  if [ "$want" == "y" ]; then
    buildTelegram
  fi
else
  buildTelegram
fi
cd ${PROOT}
if [ ! -e "${PROOT}/assets/telegram/.telegram-cli/auth" ]
then
  echo "Now you need to setup telegram account..."
  ./telegram.sh
fi
echo "All set! Now you can run ./gateway.sh script from here!"
