TARGET=gateway
VFLAGS=-version=VibeDefaultMain -version=VibeLibeventDriver -version=OPENSSL -version=Have_evilcms -version=Have_vibe_d -version=Have_libevent -version=Have_openssl 
LIBS=-L./lib/libvibe-d.so -L-ldl -L-levent_pthreads -L-levent -L-lssl -L-lcrypto -defaultlib=libphobos2.so

INCLUDES=-I. -Isrc -Iinclude
#VIEW_INCLUDES=-Jviews
CFLAGS=-fPIC -w ${VFLAGS}
LDFLAGS=-L--no-as-needed -L-s
SOURCES=./include/vibe/appmain.d ./src/kernel.d ./src/main.d src/robocop.d
TMP_DIR=./.tmp/
BIN_DIR=./bin/
OBJECTS=${TARGET}.o

PREFIX=

all:app mongo-client modz appz
app:
	@echo Building Gateway BUS Project
	dmd -c -of${TMP_DIR}${OBJECTS} ${VIEW_INCLUDES} ${INCLUDES} ${CFLAGS} ${SOURCES}
	dmd -of${BIN_DIR}${TARGET} ${TMP_DIR}${OBJECTS} ${LDFLAGS} ${LIBS}
modz:
	@echo Building Gateway BUS Project Modules
	make -C src/modules
mongo-client:
	@echo Building Gateway BUS Project Mongo
	make -C mongo/
appz:
	@echo No apps to build
clean:
	rm ${BIN_DIR}* ${TMP_DIR}*

run:
	${BIN_DIR}${TARGET}
	
install:
