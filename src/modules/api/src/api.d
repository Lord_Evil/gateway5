import vibe.d;
import std.stdio;
import std.datetime : Clock, stdTimeToUnixTime;
import std.digest.sha;
import std.uni;

string[2] mod_name = ["_API_","api"];
string[] mod_routes=["json"];
Json function(string,Json=Json.emptyObject) appFireEvent;

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) int init() {
	logInfo("webapi: Initializing!");
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) string[] routes() {
	return mod_routes;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) //for some reason params come in reverse order when called from kernel
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath=="/json"){
		res.headers["Access-Control-Allow-Origin"]="*";
		bool apiLogin=false;
		string content=req.bodyReader.readAllUTF8();
		if(content.length<1) content=req.json.toString();
		if(config.global.login.get!bool){
			//use hostsAllowed config setting
			if(config.global.hostsAllowed.type==Json.Type.Array){
				//writeln("Incomming API Call from: "~req.clientAddress.toAddressString);
				foreach(Json host;config.global.hostsAllowed){
					if(req.clientAddress.toAddressString==host.get!string){
						apiLogin=true;
					}
				}
			}
			if(("conv_signature" in req.query)!=null&& ("conv_time" in req.query)!=null){
				string api_secret=config.global.api_secret.get!string;
				if(toHexString(sha1Of(req.query["conv_time"]~api_secret~content~api_secret)).toLower==req.query["conv_signature"]){
					apiLogin=true;
				}else{
					logInfo("########External API Call signature verification failed!");
					return;
				}
			}
			if(!apiLogin)
			{
				if (!req.session || !req.session.get!bool("auth")){ 
					res.writeJsonBody(["res":"FAIL","message":"Must login first!"]);
					return;
				}
				//check whether user session is expired
				long activeT = req.session.get!long("active");
				long currentT = Clock.currTime().toUnixTime();
				if(activeT+config.global["timeout"].get!long<currentT){
					res.terminateSession();
					res.writeJsonBody(["res":"FAIL","message":"Session expired! Must login!"]); return;
				}
				req.session.set("active",Clock.currTime().toUnixTime());
			}
		}
		
		Json data = Json.emptyObject;
		data.data = Json.emptyObject;
		if(config.global.login.get!bool && !apiLogin){
			data.data.user = Json.emptyObject;
			data.data.user.userID=req.session.get!string("userid");
			data.data.user.social=req.session.get!string("social");
			data.data.user.ip=req.peer;
			data.data.user.userRole=req.session.get!string("role");
		}else{
			data.data.user = Json.emptyObject;
			data.data.user.userID="user#emptyObject";
			data.data.user.ip=req.peer;
			data.data.user.userRole="admin";
		}

		try{
			res.writeJsonBody(parseJSON(parseJsonString(content),data));
		}
		catch(Exception e){
			logInfo("Res write failed.. "~e.msg);
		}
	}
}
Json parseJSON(Json msg, Json data){
	Json reply=Json.emptyObject;
	string sw;
	string req;
	data.from=mod_name[1];
	try{
		if(msg["req"].to!string!="undefined"){
			req = msg["req"].get!string;
		}else{
			reply.rep="FAIL";
			reply.msg="Empty 'req'";
			return reply;
		}
		if(msg["data"]["switch"].type==Json.Type.string){
			sw = msg["data"]["switch"].get!string;
			data.data["switch"]=sw;
		}else{
			reply.rep="FAIL";
			reply.msg="Empty 'switch'";
			return reply;
		}
		if(msg["data"]["params"].type==Json.Type.object){
			data.data["params"]=msg["data"]["params"];
		}
		if(msg["to"].to!string!="undefined"){
			data["to"]=msg["to"];
		}
		if(msg["netmod_to"].to!string!="undefined"){
			data["netmod_to"]=msg["netmod_to"];
		}
		reply=appFireEvent(req,data);

		return reply;
	}
	catch(Exception e){
		logInfo(e.msg);
		reply.rep="BAD_JSON";
		return reply;
	}

}
shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
	}


shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
}