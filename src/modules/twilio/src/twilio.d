import vibe.d;
import vibe.data.json;
import vibe.utils.array;
import vibe.inet.url;
import std.stdio;
import std.uri;
import std.conv;
import std.variant;
import std.datetime;

import vibe.templ.parsertools;

string[2] mod_name = ["_SMS_","sms"]; // 1 - for logs, 2 - for calls
string[] mod_hooks=["_SOCIAL_ACTION_"];

string[] mod_routes=["smsin"]; // stands for https://chat.ingenia.name/twilio/smsin

//TODO: process more events

extern (C) string[] routes() {
	return mod_routes;
}



Json function(string,Json=Json.emptyObject) appFireEvent;

Json config;
string key;
string username;
string from_number;
string twilio_url;

extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) int init() {
	logInfo("twilio sms: Initializing!");
	twilio_url=config["mod"]["url"].get!string;
	from_number=config["mod"]["number"].get!string;
	username=config["mod"]["username"].get!string;
	key=config["mod"]["key"].get!string;
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) string[] hooks() {
	return mod_hooks;
}
extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	Json params=Json.emptyObject;
	if(data["data"]["params"].type==Json.Type.object){
		params=data["data"]["params"];
	}
	try{
		switch(data["data"]["switch"].get!string){
			case "IDENTIFY": //returns it's name
				j.data=Json.emptyObject;
				j.data.name=mod_name[1];
				break;
			case "GET_MESSAGES": //returns last ten messages. TODO: add option to load dialog tops (more messages)
				j.data=getMessages(params["userID"].get!string);
				break;
			case "SEND_MESSAGE": //sends message. TODO: images, files, documents, maps
				j.data = sendMessage(params["userID"].get!string,params["message"].get!string);
				break;
			case "ADD_CONTACT":
				addContact(params["userID"].get!string,params["fname"].get!string,params["lname"].get!string);
				break;
			default:
				j.data="BAD_REQUEST";
				break;
		}
		logInfo("#################SMS EVENT IN");
		logInfo("R: "~data["data"].toString());
		logInfo("A: "~j.data.toString());
		logInfo("################END SMS EVENT");
	}catch(Exception e){
		logInfo("#################SMS EVENT IN");
		logInfo("R: "~data["data"].toString());
		logInfo("SMS command failed!");
		logInfo("A: "~e.msg);
		logInfo("################END SMS EVENT");
	}
	j.from=mod_name[1];
	j.res="OK";
	return j;
}


//HTTPClientRequest 
void createSMSSendRequest (HTTPClientRequest smsreq, string number, string message) {

	smsreq.method = HTTPMethod.POST;
	smsreq.addBasicAuth(username, key);
	smsreq.writeFormBody(["To":number,"From":from_number,"Body":message]);
}

Json readResponse (HTTPClientResponse res) {
/*
{"account_sid":"ACd73c27615a79f0355dadf90027dd2e24","uri":"/2010-04-01/Accounts/ACd73c27615a79f0355dadf90027dd2e24/SMS/Messages/
SMcc9b2cb7bc524766a28c09a1b4b8a5e7.json","date_sent":null,"price":null,"num_segments":"1","sid":"SMcc9b2cb7bc524766a28c09a1b4b8a5e7",
"date_updated":"Tue, 19 May 2015 09:39:52 +0000","from":"+15005550006","body":"Hello from web / Twilio 30","status":"queued",
"price_unit":"USD","date_created":"Tue, 19 May 2015 09:39:52 +0000","to":"+15005550006","direction":"outbound-api","api_version":"2010-04-01"}

status - what is important for us.
*/
	Json js = res.readJson;
	writeln ("TWILIO RESPONSE: " ~ js.toString);
//	logInfo ("TWILIO RESPONSE: " ~ js.toString);
	logInfo("Response: %d", res.statusCode);
	return js;
}
Json sendMessage (string number, string message) {
	Json response = Json.emptyObject;
	if (true) { // if number is valid  / length of the message etc ??
		try{
			writeln ("Requesting TWILIO: " ~ number ~ ", msg: " ~ message ~ ", from: " ~ from_number);
			logInfo ("Requesting TWILIO: ");
			URL url = URL(twilio_url);
			requestHTTP(url,
				(scope req) {
					createSMSSendRequest (req, number, message);
				},
				(scope res) {
					response = readResponse (res);
					if(response.status.get!string=="queued"){
						saveMessage(number, "from", message,response.sid.get!string);
					}
				}
			);
		}catch(Exception e){
			logInfo("TWILIO - _PUSH_MESSAGE_ exception");
			logInfo(e.msg);
			return cast(Json) e.msg;
		}
	}
	return response;
}

extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath=="/smsin"){
		//res.headers["Access-Control-Allow-Origin"]="*";
		try{
			string st = "TWILIO INCOMING SMS: json: " ~ req.json.toString~", req.form.length: "~ text(req.form.length); 
/* Item: ToCountry, val: CA
Item: ToState, val: Alberta
Item: SmsMessageSid, val: SMd5295620caa00818f7277e3f752489ec
Item: NumMedia, val: 0
Item: ToCity, val: Calgary
Item: FromZip, val:
Item: SmsSid, val: SMd5295620caa00818f7277e3f752489ec
Item: FromState, val:
Item: SmsStatus, val: received
Item: FromCity, val:
Item: Body, val: Ггггы
Item: FromCountry, val: NZ
Item: To, val: +15873176270
Item: ToZip, val:
Item: MessageSid, val: SMd5295620caa00818f7277e3f752489ec
Item: AccountSid, val: AC6bf4a3c8f972195daf8f52b8bc5306e5
Item: From, val: +642108211923
Item: ApiVersion, val: 2010-04-01
*/

			logInfo(st);
			saveMessage(req.form["From"].to!string,"to", req.form["Body"].to!string,req.form["SmsSid"].to!string);
		}catch(Exception e){
			logInfo(e.msg);
		}
		
	}
	//res.writeBody("OK");
	res.writeVoidBody();
}
void addContact(string userID, string fname, string lname){
	Json data = Json.emptyObject;
	data.data=Json.emptyObject;
	data.data["switch"]="UPDATE_PROFILE";
	data.data.params=Json.emptyObject;
	data.data.params["userID"]=userID;
	data.data.params["upkey"]="clientname";
	data.data.params["upval"]=fname~" "~lname;
	data.data.params["social"]="sms";
	data["to"]="pybot";
	data["from"]=mod_name[1];
	appFireEvent("_DB_ACTION_",data);
}
void saveMessage(string userid, string way, string message, string sid){
	string time = Clock.currTime().toUnixTime().to!string;
	Json data = Json.emptyObject;
	data.data=Json.emptyObject;
	data.data["switch"]="SAVE_MESSAGE";
	data.data.params=Json.emptyObject;
	data.data.params["userID"]=userid;
	data.data.params["way"]=way;
	data.data.params["data"]=message;
	data.data.params["time"]=time;
	data.data.params["id"]=sid;
	data.data.params["account"]=config.mod.account.get!string;
	data.data.params["number"]=config.mod.number.get!string;
	data.data.params["type"]="text";
	data["to"]="pybot";
	data["from"]=mod_name[1];
	appFireEvent("_DB_ACTION_",data);
	data.data.remove("switch");
	data.remove("to");
	data.data.params["peer"]="";
	appFireEvent("_PUSH_MESSAGE_",data);
}
extern (C) void getRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath.indexOf("/file")>-1){
		try{
		}catch(Exception e){
			res.writeBody(e.msg);
		}
	}else{
		res.writeBody("OK");
	}
}
Json getMessages(string userid){
	Json data = Json.emptyObject;
	data.data=Json.emptyObject;
	data.data["switch"]="LIST_MESSAGES";
	data.data.params=Json.emptyObject;
	data.data.params["userID"]=userid;
	data["to"]="pybot";
	data["from"]=mod_name[1];
	return appFireEvent("_DB_ACTION_",data);
}

shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
}
