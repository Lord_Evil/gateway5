module dbdocument;

import vibe.d;
import std.stdio;
import std.conv;

class dbDocument{
	private MongoCollection collection;
		//adds new object to db.collection
	public void addDoc(string key, string value){
		if(key.length==0&&value.length>0){
			collection.insert(serializeToBson(parseJsonString(value)));
		}else{
			collection.insert([key:value]);
		}
	}

	public void updateRange(string query, string upkey, string upval){
		try{
			Bson m_query = serializeToBson(parseJsonString(query));
			collection.update(m_query,["$set":[upkey:upval]],UpdateFlags.MultiUpdate);
		}catch{
			logInfo("Bad Json in query");
		}
	}
	//rewrites/updates object db.collection.document
	public void updateDoc(string key, string value,string upkey,string upval){
		Bson doc;
		Bson upvalJ;
		bool isUpvalJson;
		bool single; //case updating whole object
		if(upkey.length>0) single=false;
		else single=true;

		try{
			upvalJ=serializeToBson(parseJsonString(upval));
			isUpvalJson=true;
		}catch{
			isUpvalJson=false;
		}
		if(key=="_id"){
			BsonObjectID id = BsonObjectID.fromString(value);
			doc = collection.findOne([key:id]);
			if(doc is Bson(null)) {
				return;
			}
		}else{
			doc = collection.findOne([key:value]);
		}
		if(doc is Bson(null)) {
		//some black magic here - if object does not exist then we create a new one and create a full path for the updating document:value
			doc=Bson.emptyObject;
			string[] path=key.split(".");
			Bson newobj;
			Bson latest = value;
			for(int i=cast(int)path.length-1;i>-1;i--){
				newobj=Bson.emptyObject;
				newobj[path[i]]=latest;
				latest=newobj;
			}
			doc=latest;
			collection.update([key:value],doc,UpdateFlags.Upsert);
		}
		//insert whole object
		if(single){
			if(isUpvalJson)
				if(key=="_id"){
					BsonObjectID id = BsonObjectID.fromString(value);
					collection.findAndModify([key:id],["$set":upvalJ]);
				}else{
					collection.findAndModify([key:value],["$set":upvalJ]);
				}
			else
				logInfo("ERROR - EMPTY KEY");
			return;
		}
		//more black magic - set any value for any key even if it does not exist it will be created;
		string[] path=upkey.split(".");
		if(path.length>1){
			Bson ret=doc;
			string[] newpath;
			for(int i=0;i<path.length;i++){
				if(ret.opIndex(path[i])!=Bson(null)){
					ret=ret.opIndex(path[i]);
						newpath~=path[i];
				}else{
					try{
						newpath~=path[i];
					//	logInfo("Trying to set path "~newpath.join("."));
						if(key=="_id"){
							BsonObjectID id = BsonObjectID.fromString(value);
							ret=collection.findAndModify([key:id],["$set":[(newpath.join(".")):Bson.emptyObject]]);
						}else{
							ret=collection.findAndModify([key:value],["$set":[(newpath.join(".")):Bson.emptyObject]]);
						}
					}catch(Exception e){
						logInfo("Failed: "~e.msg);
					}
				}
			}
			try{
				if(isUpvalJson)
					if(key=="_id"){
						BsonObjectID id = BsonObjectID.fromString(value);
						collection.findAndModify([key:id],["$set":[newpath.join("."):upvalJ]]);
					}else{
						collection.findAndModify([key:value],["$set":[newpath.join("."):upvalJ]]);
					}
				else
					if(key=="_id"){
						BsonObjectID id = BsonObjectID.fromString(value);
						collection.findAndModify([key:id],["$set":[newpath.join("."):upval]]);
					}else{
						collection.findAndModify([key:value],["$set":[newpath.join("."):upval]]);
					}
			}catch(Exception e){
				logInfo("Failed: "~e.msg);
			}
		}else{
			if(isUpvalJson){
				doc[path[0]]=upvalJ;
				if(key=="_id"){
					BsonObjectID id = BsonObjectID.fromString(value);
					collection.update([key:id],["$set":[path[0]:upvalJ]],UpdateFlags.Upsert);
				}else{
					collection.update([key:value],["$set":[path[0]:upvalJ]],UpdateFlags.Upsert);
				}
			}
			else{
				doc[path[0]]=upval;
				if(key=="_id"){
					BsonObjectID id = BsonObjectID.fromString(value);
					collection.update([key:id],["$set":[path[0]:upval]],UpdateFlags.Upsert);
				}else{
					collection.update([key:value],["$set":[path[0]:upval]],UpdateFlags.Upsert);
				}
			}
		}
	}

	public Json getDoc(string key, string value){
		//TODO: value as object
		Json resp = Json.emptyObject;
		Bson doc;
		if(key=="_id"){
			BsonObjectID id = BsonObjectID.fromString(value);
			doc = collection.findOne([key:id]);
		}else{
			doc = collection.findOne([key:value]);
		}
		if(doc!=Bson(null)){
			resp = doc.toJson();
		}else{
			resp=Json.emptyObject;
		}
		return resp;
	}
	public Json getDocs(string key, string value){
		Json resp = Json.emptyObject;
		Json[] docs;

		bool isValJson;
		Bson valJ;
		try{ //check whether value is an object nor string
			valJ=serializeToBson(parseJsonString(value));
			isValJson=true;
		}catch{
			isValJson=false;
		}

		if(isValJson){
			if(key.length>0){ //if query has only one selector, e.g.: db.client.find({sex:"m"})
				auto cursor = collection.find([key:valJ]);
				while(!cursor.empty){
					docs~=cursor.front().toJson();
					cursor.popFront();
				}
			}else{ //for query with multiple selectors, e.g.: db.client.find({sex:"m", language:"ru"})
				auto cursor = collection.find(valJ);
				while(!cursor.empty){
					docs~=cursor.front().toJson();
					cursor.popFront();
				}
			}
		}else{
			if(key.length>0 && value.length>0){
				auto cursor = collection.find([key:value]);
				while(!cursor.empty){
					docs~=cursor.front().toJson();
					cursor.popFront();
				}
			}else{
				auto cursor = collection.find();
				while(!cursor.empty){
					docs~=cursor.front().toJson();
					cursor.popFront();
				}
			}
		}

		if(docs.length>0){
			resp.docs=docs;
		}else{
			resp.docs=Json.emptyObject;
		}
		return resp;
	}
	public void removeDoc(string key, string value){
		if(key=="_id"){
			BsonObjectID id = BsonObjectID.fromString(value);
			collection.remove([key:id],DeleteFlags.SingleRemove);
		}else{
			collection.remove([key:value],DeleteFlags.SingleRemove);
		}
	}
	this(string database, string coll,MongoClient client){
		collection = client.getCollection(database~"."~coll);
	}
	~this(){
		collection.destroy();
	}
}
