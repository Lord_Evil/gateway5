import vibe.d;
import std.stdio;
import std.conv;
import dbdocument;

string[2] mod_name = ["_DB_","db"];
string[] mod_hooks=["_INSERT_DB_","_READ_DB_","_READ_ONE_DB_","_WRITE_DB_","_DELETE_DB_","_UPDATE_RANGE_DB_"];
string[] mod_routes=["pop"];

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) string[] routes() {
	return mod_routes;
}

extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	Json reply=Json.emptyObject;
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath=="/pop"){
		try{
			string event=req.json["event"].toString();
			reply=raiseEvent(event[1..$-1],req.json["data"]);
		}catch(Exception e){
			writeln(e.msg);
		}
	}
	res.writeBody(reply.toString());
}
extern (C) void getRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	res.writeBody("OK");
}

enum Actions{
	GET_DOC=1,
	GET_DOCS=2,
	INSERT_DOC=3,
	WRITE_DOC=4,
	DELETE_DOC=5,
	UPDATE_RANGE=6
}

class dbClient{
	MongoClient client;
	string database;
	public Json qDoc(string collection, string key, string value,int action){
		Json j = Json.emptyObject;
		
		dbDocument doc = new dbDocument(database, collection, client);
		switch(action){
			case Actions.GET_DOC:
				j=doc.getDoc(key,value);
				break;
			case Actions.GET_DOCS:
				j=doc.getDocs(key,value);
				break;
			case Actions.INSERT_DOC:
				doc.addDoc(key,value);
				break;
			case Actions.DELETE_DOC:
				doc.removeDoc(key,value);
				break;
			default:break;
		}
		doc.destroy();
		return j;
	}
	public Json qDoc(string collection, string key, string value,string upkey, string upval,int action){
		Json j = Json.emptyObject;
		
		dbDocument doc = new dbDocument(database, collection, client);
		switch(action){
			case Actions.WRITE_DOC:
				doc.updateDoc(key,value,upkey,upval);
				break;
			default:break;
		}
		doc.destroy();
		return j;
	}

	public Json qDoc(string collection, string value, string upkey, string upval,int action){
		Json j = Json.emptyObject;
		
		dbDocument doc = new dbDocument(database, collection, client);
		switch(action){
			case Actions.UPDATE_RANGE:
				doc.updateRange(value,upkey,upval);
				break;
			default:break;
		}
		doc.destroy();
		return j;
	}

	this(){
		database = config.mod["database"].get!string;
		string user = "";
		if(config.mod["user"].type==Json.Type.string)
			user = config.mod["user"].get!string;
		string pass="";
		if(config.mod["pass"].type==Json.Type.string)
			pass=config.mod["pass"].get!string;
		if(user.length>0 && pass.length>0)
			user~=":"~pass;
		string host=config.mod["host"].get!string;
		if(user.length>0)
			host~="@"~user;
		client = connectMongoDB("mongodb://"~host~"/");
	}
	~this(){
		delete(client);
	}
}

dbClient m_dbClient;

Json function(string,Json=Json.emptyObject) appFireEvent;

extern (C) int init() {
	logInfo(mod_name[0]~": Initializing...");
	m_dbClient = new dbClient();
	return 0;
}
extern (C) string[2] name() {
	return mod_name;
}
extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}
extern (C) string[] hooks() {
	return mod_hooks;
}
extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	logInfo(event);
	logInfo(data.toString());
	switch(event){
		case "_INSERT_DB_":
			j=m_dbClient.qDoc(data["collection"].get!string,data["key"].get!string,data["value"].get!string,Actions.INSERT_DOC);
			break;
		case "_READ_DB_":
			j=m_dbClient.qDoc(data["collection"].get!string,data["key"].get!string,data["value"].get!string,Actions.GET_DOCS);
			break;
		case "_READ_ONE_DB_":
			j=m_dbClient.qDoc(data["collection"].get!string,data["key"].get!string,data["value"].get!string,Actions.GET_DOC);
			break;
		case "_WRITE_DB_":
			j=m_dbClient.qDoc(data["collection"].get!string,data["key"].get!string,data["value"].get!string,data["upkey"].get!string,data["upval"].get!string,Actions.WRITE_DOC);
			break;
		case "_DELETE_DB_":
			j=m_dbClient.qDoc(data["collection"].get!string,data["key"].get!string,data["value"].get!string,Actions.DELETE_DOC);
			break;
		case "_UPDATE_RANGE_DB_":
			m_dbClient.qDoc(data["collection"].get!string,data["value"].get!string,data["upkey"].get!string,data["upval"].get!string,Actions.UPDATE_RANGE);
			break;
		default:break;
	}
	
	return j;
}

shared static this() {
	//logInfo(mod_name[0]~": I'm being loaded!");
	
}

shared static ~this() {
	logInfo(mod_name[1]~": I'm being unloaded!");
	m_dbClient.destroy();
}
