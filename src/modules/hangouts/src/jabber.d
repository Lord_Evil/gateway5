import strophe;
import std.stdio: writeln;

import std.conv: to;
string DStr(char* str){
    return to!string(str);
}
string DStr(const char* str){
    return to!string(str);
}
char* CStr(string str){
	if(str==null)
		return null;
	else
    	return (str~'\0').dup.ptr;
}

alias xmpp_log_level_t log_level;
alias xmpp_conn_type_t conn_type;
alias xmpp_conn_event_t conn_event;
alias xmpp_error_type_t error_type;
alias void function (void*, log_level, string, string) log_handler;
alias void delegate (void*, log_level, string, string) log_handler_d;
alias 			 void function (conn_event, int, stream_error*) conn_handler;
alias 			 void delegate(conn_event, int, stream_error*) conn_handler_d;
//alias extern (C) void function(_xmpp_conn_t*, xmpp_conn_event_t, int, xmpp_stream_error_t*, void*) xmpp_conn_handler;
alias int function (conn*, void*) timed_handler;
alias int delegate(conn*, void*) timed_handler_d;
//alias int function (conn*, stanza*, void*) jabber_handler;
//alias int delegate (conn*, stanza*, void*) jabber_handler_d;
alias int function (Stanza) jabber_handler;
alias int delegate (Stanza) jabber_handler_d;

alias _xmpp_mem_t mem;

//_xmpp_log_t
struct log
{
    log_handler _handler;
    void* userdata;
}
//xmpp_stream_error_t
struct stream_error
{
    error_type type;
    string text;
	_xmpp_stanza_t* _stanza;
}
//_xmpp_conn_t
struct conn{};
//_xmpp_ctx_t
struct ctx_t{};

alias xmpp_ctx_t Context;
class Stanza{
	private:
		Context *m_ctx;
		xmpp_stanza_t* m_stanza;
		bool custom;
		//void *operator new(size_t size, Context *ctx);
		//void operator delete(void *p);
    public:
    	this(Context *ctx){
    		custom=true;
			m_ctx=ctx;
			//m_stanza = xmpp_stanza_new(ctx->getContext());
			m_stanza = xmpp_stanza_new(ctx);
		}
		this(xmpp_stanza_t* stanza){
			if(stanza==null){
				delete this;
				return;
			}
			custom=false;
			m_stanza=stanza;
		}
		~this(){
		}
		void release(){//recommended to use function
			if(!custom){
				try{
					xmpp_stanza_release(m_stanza);
				}catch{}
			}
			destroy(this);
		}

		//Stanza clone();
		//Stanza copy();
		xmpp_stanza_t* getXmppStanza(){
			return m_stanza;
		}
		override string toString(){
			char* text;
			ulong buflen;
			xmpp_stanza_to_text (m_stanza, &text, &buflen);
			return text.DStr;
		}
		bool addChild(Stanza child){
			return cast(bool)xmpp_stanza_add_child (m_stanza, child.getXmppStanza());
		}
		Stanza[] getChildren(){
			Stanza[] children;
			xmpp_stanza_t* item;
			for (item = xmpp_stanza_get_children(m_stanza); item; item = xmpp_stanza_get_next(item)){
				children~=new Stanza(item);
			}
			return children;
		}
		Stanza getChildByName(string name){
			return new Stanza(xmpp_stanza_get_child_by_name(m_stanza,name.CStr));
		}
		Stanza getChildByNS(string ns){
			return new Stanza(xmpp_stanza_get_child_by_ns(m_stanza,ns.CStr));
		}
		Stanza getNext(){
			return new Stanza(xmpp_stanza_get_next(m_stanza));
		}
	    string getAttribute(string name){
	    	return xmpp_stanza_get_attribute (m_stanza, name.CStr).DStr;
	    }
	    //Black magic D - Style
	    string opIndex(string key){
	    	return getAttribute(key);
	    }
	    void setAttribute(string key, string value){
	    	xmpp_stanza_set_attribute (m_stanza, key.CStr, value.CStr);
	    }
	    //Black magic D - Style
	    void opIndexAssign(string value,string key){
	    	setAttribute(key, value);
	    }
		string getText(){
			return xmpp_stanza_get_text (m_stanza).DStr;
		}
		int setText(string text){
			return xmpp_stanza_set_text (m_stanza, text.CStr);
		}
		//Black magic D - Style
		void opAssign(string text){
			setText(text);
		}

		void setNamespace(string ns){
			xmpp_stanza_set_ns(m_stanza,ns.CStr);
		}
		string getNamespace(){
			return xmpp_stanza_get_ns(m_stanza).DStr;
		}
		string getName(){
			return xmpp_stanza_get_name(m_stanza).DStr;
		}
		void setName(string name){
			xmpp_stanza_set_name(m_stanza, name.CStr);
		}
		string getId(){
			return xmpp_stanza_get_id(m_stanza).DStr;
			//return this["id"]; ?
		}
		string getTo(){
			return this["to"];
		}
		string getFrom(){
			return this["from"];
		}
		string getType(){
			return xmpp_stanza_get_type(m_stanza).DStr;
			//return this["type"]; ?
		}
		void setType(string type){
			xmpp_stanza_set_type(m_stanza, type.CStr);
		}
		void setId(string id){
			xmpp_stanza_set_id(m_stanza, id.CStr);
		}
		void setTo(string to){
			this["to"]=to;
		}
		void setFrom(string from){
			this["from"]=from;
		}
		bool isText(){
			return cast(bool)xmpp_stanza_is_text(m_stanza);
		}
		bool isTag(){
			return cast(bool)xmpp_stanza_is_tag(m_stanza);
		}

//xmpp_stanza_t* xmpp_stanza_new (xmpp_ctx_t* ctx);
//xmpp_stanza_t* xmpp_stanza_clone (xmpp_stanza_t* stanza);
//xmpp_stanza_t* xmpp_stanza_copy (const xmpp_stanza_t* stanza);

//char* xmpp_stanza_get_text_ptr (xmpp_stanza_t* stanza);
//int xmpp_stanza_set_text_with_size (xmpp_stanza_t* stanza, const char* text, const size_t size);

};

//void xmpp_initialize ();
alias xmpp_initialize initialize;
//void xmpp_shutdown ();
alias xmpp_shutdown shutdown;
//int xmpp_version_check (int major, int minor);
alias xmpp_version_check version_check;
//xmpp_ctx_t* xmpp_ctx_new (const xmpp_mem_t* mem, const xmpp_log_t* log);
ctx_t* ctx_new(mem* _mem, log* _log){
	return cast(ctx_t*)xmpp_ctx_new(_mem, cast(_xmpp_log_t*)_log);
}
//void xmpp_ctx_free (xmpp_ctx_t* ctx);
void ctx_free(ctx_t* _ctx){
	xmpp_ctx_free(cast(_xmpp_ctx_t*)_ctx);
}
//xmpp_log_t* xmpp_get_default_logger (xmpp_log_level_t level);
log* get_default_logger(log_level level){
	return cast(log*)xmpp_get_default_logger(cast(xmpp_log_level_t)level);
}
//xmpp_conn_t* xmpp_conn_new (xmpp_ctx_t* ctx);
conn* conn_new(ctx_t* _ctx){
	return cast(conn*)xmpp_conn_new(cast(_xmpp_ctx_t*)_ctx);	
}
//xmpp_conn_t* xmpp_conn_clone (xmpp_conn_t* conn);
conn* conn_clone(conn* _conn){
	return cast(conn*)xmpp_conn_clone(cast(xmpp_conn_t*)_conn);
}
//int xmpp_conn_release (xmpp_conn_t* conn);
int conn_release(conn* _conn){
	return xmpp_conn_release(cast(xmpp_conn_t*)_conn);
}
//const(char)* xmpp_conn_get_jid (const xmpp_conn_t* conn);
string conn_get_jid(const conn* _conn){
	return xmpp_conn_get_jid(cast(xmpp_conn_t*)_conn).DStr;
}
//const(char)* xmpp_conn_get_bound_jid (const xmpp_conn_t* conn);
string conn_get_bound_jid(const conn* _conn){
	return xmpp_conn_get_bound_jid(cast(xmpp_conn_t*)_conn).DStr;
}
//void xmpp_conn_set_jid (xmpp_conn_t* conn, const char* jid);
void conn_set_jid(conn* _conn, string jid){
	xmpp_conn_set_jid(cast(xmpp_conn_t*)_conn, jid.CStr);
}
//const(char)* xmpp_conn_get_pass (const xmpp_conn_t* conn);
string conn_get_pass(const conn* _conn){
	return xmpp_conn_get_pass(cast(xmpp_conn_t*)_conn).DStr;
}
//void xmpp_conn_set_pass (xmpp_conn_t* conn, const char* pass);
void conn_set_pass(conn* _conn, string pass){
	return xmpp_conn_set_pass(cast(xmpp_conn_t*)_conn, pass.CStr);
}
//xmpp_ctx_t* xmpp_conn_get_context (xmpp_conn_t* conn);
ctx_t* conn_get_context(conn* _conn){
	return cast(ctx_t*)xmpp_conn_get_context(cast(xmpp_conn_t*)_conn);
}
//void xmpp_conn_disable_tls (xmpp_conn_t* conn);
void conn_disable_tls(conn* _conn){
	xmpp_conn_disable_tls(cast(xmpp_conn_t*)_conn);
}
//int xmpp_connect_client (xmpp_conn_t* conn, const char* altdomain, ushort altport, xmpp_conn_handler callback, void* userdata);
int connect_client(conn* _conn, string altdomain, int altport, conn_handler callback, void* userdata){
	//return xmpp_connect_client(cast(xmpp_conn_t*)_conn, cast(const char*)altdomain.CStr, altport, callback, userdata);
	return xmpp_connect_client(cast(xmpp_conn_t*)_conn, altdomain!=null?cast(const char*)altdomain.CStr:null, cast(ushort)altport, get_conn_handler(callback), userdata);
}
int connect_client(conn* _conn, string altdomain, int altport, conn_handler_d callback, void* userdata){
	//return xmpp_connect_client(cast(xmpp_conn_t*)_conn, cast(const char*)altdomain.CStr, altport, callback, userdata);
	return xmpp_connect_client(cast(xmpp_conn_t*)_conn, altdomain!=null?cast(const char*)altdomain.CStr:null, cast(ushort)altport, get_conn_handler_d(callback), userdata);
}
extern (C) xmpp_conn_handler get_conn_handler(conn_handler callback){
	static conn_handler handler;
	handler=callback;
	extern (C) void xmpp_callback(_xmpp_conn_t* r_conn, xmpp_conn_event_t event, int status, xmpp_stream_error_t* s_error, void* r_userdata){
		 //handler(cast(conn*)r_conn, event, status, cast(stream_error*)s_error, r_userdata);
		 handler(event, status, cast(stream_error*)s_error);
	}
	return &xmpp_callback;
}
extern (C) xmpp_conn_handler get_conn_handler_d(conn_handler_d callback){
	static conn_handler_d handler;
	handler=callback;
	extern (C) void xmpp_callback(_xmpp_conn_t* r_conn, xmpp_conn_event_t event, int status, xmpp_stream_error_t* s_error, void* r_userdata){
		 handler(event, status, cast(stream_error*)s_error);
	}
	return &xmpp_callback;
}
//int xmpp_connect_component (xmpp_conn_t* conn, const char* server, ushort port, xmpp_conn_handler callback, void* userdata);
//void xmpp_disconnect (xmpp_conn_t* conn);
void disconnect(conn* _conn){
	xmpp_disconnect(cast(xmpp_conn_t*)_conn);
}
//void xmpp_free (const xmpp_ctx_t* ctx, void* p);
//void xmpp_send (xmpp_conn_t* conn, xmpp_stanza_t* stanza);
//void xmpp_send_raw_string (xmpp_conn_t* conn, const char* fmt, ...);
//void xmpp_send_raw (xmpp_conn_t* conn, const char* data, const size_t len);
//void xmpp_timed_handler_add (xmpp_conn_t* conn, xmpp_timed_handler handler, const c_ulong period, void* userdata);
//void xmpp_timed_handler_delete (xmpp_conn_t* conn, xmpp_timed_handler handler);
//void xmpp_handler_add (xmpp_conn_t* conn, xmpp_handler handler, const char* ns, const char* name, const char* type, void* userdata);
	void handler_add(conn* _conn, jabber_handler callback, string ns, string name, string type, void* userdata){
		xmpp_handler_add (cast(xmpp_conn_t*)_conn, get_xmpp_handler(callback), ns.CStr, name.CStr, type.CStr, cast(xmpp_ctx_t*)userdata);
	}
	void handler_add(conn* _conn, jabber_handler_d callback, string ns, string name, string type, void* userdata){
		xmpp_handler_add (cast(xmpp_conn_t*)_conn, get_xmpp_handler_d(callback), ns.CStr, name.CStr, type.CStr, cast(xmpp_ctx_t*)userdata);
	}
	extern (C) xmpp_handler get_xmpp_handler(jabber_handler callback){
		static jabber_handler handler;
		handler=callback;
		extern (C) int xmpp_callback(xmpp_conn_t* _conn, xmpp_stanza_t* _stanza, void* userdata){
			return handler(new Stanza(_stanza));
		}
		return &xmpp_callback;
	}
	extern (C) xmpp_handler get_xmpp_handler_d(jabber_handler_d callback){
		static jabber_handler_d handler;
		handler=callback;
		extern (C) int xmpp_callback(xmpp_conn_t* _conn, xmpp_stanza_t* _stanza, void* userdata){
			return handler(new Stanza(_stanza));
		}
		return &xmpp_callback;
	}
//void xmpp_handler_delete (xmpp_conn_t* conn, xmpp_handler handler);
//void xmpp_id_handler_add (xmpp_conn_t* conn, xmpp_handler handler, const char* id, void* userdata);
	void id_handler_add(conn* _conn, jabber_handler callback, string id, void* userdata){
			xmpp_id_handler_add (cast(xmpp_conn_t*)_conn, get_xmpp_handler(callback), id.CStr, cast(xmpp_ctx_t*)userdata);
	}
	void id_handler_add(conn* _conn, jabber_handler_d callback, string id, void* userdata){
			xmpp_id_handler_add (cast(xmpp_conn_t*)_conn, get_xmpp_handler_d(callback), id.CStr, cast(xmpp_ctx_t*)userdata);
	}
//void xmpp_id_handler_delete (xmpp_conn_t* conn, xmpp_handler handler, const char* id);






//void xmpp_run_once (xmpp_ctx_t* ctx, const c_ulong timeout);
//void xmpp_run (xmpp_ctx_t* ctx);
void run(ctx_t* _ctx){
	xmpp_run(cast(xmpp_ctx_t*)_ctx);
}
//void xmpp_stop (xmpp_ctx_t* ctx);
void stop(ctx_t* _ctx){
	xmpp_stop(cast(xmpp_ctx_t*)_ctx);
}