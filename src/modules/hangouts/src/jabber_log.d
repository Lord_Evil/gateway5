import core.stdc.stdarg;
import vibe.core.log: logInfo, logDebug, logWarn, logError;
import jabber;

struct jabber_log{
	log_handler handler;
    void *userdata;
    this(log_level level){
    	switch(level){
    		case log_level.INFO:
    		break;
    		case log_level.DEBUG:
    		break;
    		case log_level.WARN:
    		break;
    		case log_level.ERROR:
    		break;
    		default:break;
    	}
    }
    void _error(ctx_t * ctx, char * area, char * fmt,	...){}
	void _warn(ctx_t * ctx, char * area, char * fmt, ...){}
	void _info(ctx_t * ctx, char * area, char * fmt, ...){}
	void _debug(ctx_t * ctx, char * area, char * fmt, ...){}

	void _log(ctx_t * ctx, log_level level, char * area, char * fmt, va_list ap){}
}