import vibe.data.json;
import vibe.http.server;
import vibe.core.log: logInfo, logDebug;
import vibe.http.client;

import core.thread;
import std.string;
import std.datetime;

import strophe;
import jabber;

string[2] mod_name = ["_HANGOUTS_","hangouts"];
string[] mod_hooks=["_SOCIAL_ACTION_"];
string[] mod_routes=["push","save"];

extern (C) string[] routes() {
	return mod_routes;
}

Json function(string,Json=Json.emptyObject) appFireEvent;

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

class XMPPClient: Thread
{
    this(int p, string jid, string pass, string host)
    {
    	port = p;
    	_jid=jid;
    	_pass=pass;
    	_host=host;
        super( &runClient );
    }
    ~this(){
    }
    void runClient(){
    	initialize();
	    //DEBUG:
	    //_log = get_default_logger(log_level.DEBUG);
	    _log = get_default_logger(log_level.INFO);
	    m_ctx = ctx_new(null, _log);
	    m_conn = conn_new(m_ctx);
	    conn_set_jid(m_conn, _jid);
	    conn_set_pass(m_conn, _pass);
	    connect_client(m_conn, _host, 0, (&conn_handler), m_ctx);
	    jabber.run(m_ctx);
    }


	void sendMessage(string userID, string message){
		Json params = Json.emptyObject;

		Stanza msg=new Stanza(cast(xmpp_ctx_t*)m_ctx);
		msg.setName("message");
		msg.setType("chat");
		//xmpp_stanza_set_id(msg, "message1");
		msg["to"]=userID;

		Stanza m_body=new Stanza(cast(xmpp_ctx_t*)m_ctx);
		m_body.setName("body");
		
		Stanza text=new Stanza(cast(xmpp_ctx_t*)m_ctx);
		text=message;

		m_body.addChild(text);
		text.release();
		msg.addChild(m_body);
		m_body.release();

		xmpp_send(cast(xmpp_conn_t*)m_conn, msg.getXmppStanza);
		msg.release();

		params["userID"]=userID;
		params["peer"]="";
		params["way"]="from";
		params["data"]=message;
		string time = Clock.currTime().toUnixTime().to!string;
		params["time"]=time;
		params["id"]="";
		params["type"]="text";
		saveMessage(params);

		Json lmsg=Json.emptyObject;
		lmsg.data=Json.emptyObject;
		lmsg.data.params=params;
		lmsg.from=mod_name[1];
		loopFire("_PUSH_MESSAGE_", lmsg,"push");

	}
	void getRoster(){
		Stanza iq=new Stanza(cast(xmpp_ctx_t*)m_ctx);
		iq.setName("iq");
		iq.setType("get");
		iq.setId("roster1");

		Stanza query=new Stanza(cast(xmpp_ctx_t*)m_ctx);
		query.setName("query");
		query.setNamespace(NS_ROSTER);

		iq.addChild(query);
		query.release();
		id_handler_add(m_conn, &handle_roster, "roster1", m_ctx);
		xmpp_send(cast(xmpp_conn_t*)m_conn, iq.getXmppStanza());
		iq.release();
	}

private:
	int port;//local port
	ctx_t* m_ctx;
	conn* m_conn;
	string _jid, _pass, _host;
	log* _log;
	Json[] m_roster;

	void _stop(){
		logInfo("Stopping xmpp loop..");
		disconnect(m_conn);
		conn_release(m_conn);
		stop(m_ctx);
    	ctx_free(m_ctx);
    	shutdown();
    	logInfo("Stoppped!");
	}
	Json[] getContacts(){
		return m_roster;
	}
	int handle_roster(Stanza stanza)
	{
		if(stanza.getType=="error"){
			logInfo("ERROR: get roster query failed");
			return 1;
		}
		auto nodes=stanza.getChildByName("query").getChildren;
		//DEBUG:
		//logInfo("###################ROSTER");
		Json[] contacts;
		foreach(Stanza node; nodes){
			//DEBUG:
			//logInfo(node["name"]~":"~node["jid"]~":sub="~node["subscription"]);
			Json contact=Json.emptyObject;
			contact.userID=node["jid"];
			contact.name=node["name"].length>0?node["name"]:node["jid"][0..node["jid"].indexOf("@")];
			contacts~=contact;
			//test opop assign attributes
			//node["name"]="New Name";
			//test op assign setText and adding children
			/*
			Stanza test=new Stanza(cast(xmpp_ctx_t*)m_ctx);
			test="testtest";
			node.addChild(test);
			*/
			//DEBUG:
			//writeln(contact);
			//writeln(node);
		}
		m_roster=contacts;
		//DEBUG:
		//logInfo("###################END ROSTER");
		handler_add(m_conn,&inMessage, null, "message", null, m_ctx);
	    return 1;
	}
	/*
	int version_handler(Stanza _stanza)
	{
		xmpp_stanza_t* reply, query, name, _version, text;
		char *ns;
		xmpp_ctx_t *ctx = cast(xmpp_ctx_t*)m_ctx;
		logInfo("Received version request from %s\n", xmpp_stanza_get_attribute(cast(xmpp_stanza_t*)_stanza, "from"));
		
		reply = xmpp_stanza_new(ctx);
		xmpp_stanza_set_name(reply, "iq");
		xmpp_stanza_set_type(reply, "result");
		xmpp_stanza_set_id(reply, xmpp_stanza_get_id(cast(xmpp_stanza_t*)_stanza));
		xmpp_stanza_set_attribute(reply, "to", xmpp_stanza_get_attribute(cast(xmpp_stanza_t*)_stanza, "from"));
		
		query = xmpp_stanza_new(ctx);
		xmpp_stanza_set_name(query, "query");
	    ns = xmpp_stanza_get_ns(xmpp_stanza_get_children(cast(xmpp_stanza_t*)_stanza));
	    if (ns) {
	        xmpp_stanza_set_ns(query, ns);
	    }

		name = xmpp_stanza_new(ctx);
		xmpp_stanza_set_name(name, "name");
		xmpp_stanza_add_child(query, name);
		
		text = xmpp_stanza_new(ctx);
		xmpp_stanza_set_text(text, "Chatney in Hangouts");
		xmpp_stanza_add_child(name, text);
		
		_version = xmpp_stanza_new(ctx);
		xmpp_stanza_set_name(_version, "version");
		xmpp_stanza_add_child(query, _version);
		
		text = xmpp_stanza_new(ctx);
		xmpp_stanza_set_text(text, "1.0");
		xmpp_stanza_add_child(_version, text);
		
		xmpp_stanza_add_child(reply, query);

		xmpp_send(cast(xmpp_conn_t*)m_conn, reply);
		xmpp_stanza_release(reply);
		return 1;
	}
*/
	int inMessage(Stanza stanza){
		//writeln(stanza.getChildByName("body"));
		if(!stanza.getChildByName("body")) return 1;
		if(stanza["type"]=="" || stanza["type"]=="error") return 1;
		string intext=stanza.getChildByName("body").getText;
		string jid = stanza["from"];
		logInfo("Incoming message from %s: %s\n", jid, intext);

		Json msg=Json.emptyObject;
		msg.data=Json.emptyObject;
		msg.data.params=Json.emptyObject;
		msg.data.params["userID"]=jid.indexOf("/")>-1?jid[0..jid.indexOf("/")]:jid;
		msg.data.params["peer"]=jid[0..jid.indexOf("@")];
		msg.data.params["way"]="to";
		msg.data.params["data"]=intext;
		string time = Clock.currTime().toUnixTime().to!string;
		msg.data.params["time"]=time;
		msg.data.params["id"]=stanza["id"];
		msg.data.params["type"]="text";

		msg.from=mod_name[1];
		saveMessage(msg.data.params);
		loopFire("_PUSH_MESSAGE_", msg,"push");
		return 1;
	}
	void conn_handler(conn_event event, int status, stream_error* s_error){
		xmpp_stanza_t *iq;
		xmpp_stanza_t *query;
		if (status == conn_event.CONNECT) {
	        logDebug("XMPP: connected\n");
			//handler_add(m_conn,&version_handler, "jabber:iq:version", "iq", null, m_ctx);

	        xmpp_stanza_t* pres = xmpp_stanza_new(cast(xmpp_ctx_t*)m_ctx);
			xmpp_stanza_set_name(pres, "presence");
			xmpp_send(cast(xmpp_conn_t*)m_conn, pres);
			xmpp_stanza_release(pres);

			getRoster();

	    }
	    else {
	        logInfo("XMPP: disconnected\n");
	        _stop();
	    }
	}
	void saveMessage(Json params){
		Json data = Json.emptyObject;
		data.data=Json.emptyObject;
		data.data["switch"]="SAVE_MESSAGE";
		data.data.params=params;
		data["to"]="pybot";
		data["from"]=mod_name[1];
		loopFire("_DB_ACTION_",data, "save");
    }
	void loopFire(string event,Json data, string route){
		void fire(){
			try{
				requestHTTP("http://localhost:" ~port.to!string~ "/hangouts/"~route,
					(scope req) {
						req.method = HTTPMethod.POST;
						req.writeJsonBody(["data":data]);
					},
					(scope res) {
					}
				);
			}catch(Exception e){
				logInfo(mod_name[1]~" - _PUSH_MESSAGE_");
				logInfo("PUSH failed: "~e.msg);
			}
		}
		new Thread(&fire).start();
	}
}


XMPPClient comm;

extern (C) int init() {
	logInfo(mod_name[1]~": Initializing!");
	

    string jid=config.mod["jid"].get!string;
    string pass=config.mod["pass"].get!string;
    string host=config.mod["host"].type!=Json.Type.Null?config.mod["host"].get!string:null;
    
    comm=new XMPPClient(config.global["port"].get!int,jid,pass,host);
    comm.start();

	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) string[] hooks() {
	return mod_hooks;
}

extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	Json params=Json.emptyObject;
	if(data["data"]["params"].type==Json.Type.object){
		params=data["data"]["params"];
	}
	try{
		switch(data["data"]["switch"].get!string){
			case "IDENTIFY": //returns it's name
				j.data=Json.emptyObject;
				j.data.name=mod_name[1];//TODO: return module name, messanger type, client ID
				break;
			case "GET_CONTACTS": //returns list of contacts
				j.data=Json.emptyObject;
				j.data.list=comm.getContacts();
				break;
			case "GET_DIALOGS": //returns list of dialogs and unread messages
				j.data=Json.emptyObject;
				break;
			case "GET_MESSAGES": //returns last ten messages. TODO: add option to load dialog tops (more messages)
				j.data=getMessages(params["userID"].get!string);
				break;
			case "GET_LOCAL_MESSAGES":
				j.data=Json.emptyObject;
				break;
			case "SEND_MESSAGE": //sends message. TODO: images, files, documents, maps
				//sendMessage(params["userID"].get!string,params["message"].get!string);
				comm.sendMessage(params["userID"].get!string,params["message"].get!string);
				break;
			case "GET_FILE":
				//getFile(params["msgID"].get!string,params["type"].get!string); ?
				break;
			case "ADD_CONTACT":
				//addContact(params["userID"].get!string,params["fname"].get!string,params["lname"].get!string);
				break;
			//TODO: update contact, delete contact
			case "DELETE_CONTACT":
				//deleteContact(params["userID"].get!string);
				break;
			default:
				j.data="BAD_REQUEST";
				break;
		}
	}catch(Exception e){
		logInfo(mod_name[0]~": "~e.msg);
	}
	j.from=mod_name[1];
	j.res="OK";
	return j;
}

Json getMessages(string userid){
	Json data = Json.emptyObject;
	data.data=Json.emptyObject;
	data.data["switch"]="LIST_MESSAGES";
	data.data.params=Json.emptyObject;
	data.data.params["userID"]=userid;
	data["to"]="pybot";
	data["from"]=mod_name[1];
	return appFireEvent("_DB_ACTION_",data);
}

extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath=="/push"){
		//res.headers["Access-Control-Allow-Origin"]="*";
		try{
			appFireEvent("_PUSH_MESSAGE_",req.json["data"]);
		}catch(Exception e){
			logInfo(e.msg);
		}
		
	}
	if(rpath=="/save"){
		//res.headers["Access-Control-Allow-Origin"]="*";
		try{
			appFireEvent("_DB_ACTION_",req.json["data"]);
		}catch(Exception e){
			logInfo(e.msg);
		}
	}
	res.writeBody("OK");
}

extern (C) void getRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath.indexOf("/file")>-1){ //getFile()
		try{

		}catch(Exception e){
			res.writeBody(e.msg);
		}
	}else{
		res.writeBody("OK");
	}
}
shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
	comm._stop();
	comm.join();
}
