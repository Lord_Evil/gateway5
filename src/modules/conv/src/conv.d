import vibe.d;
import vibe.data.json;
import std.stdio;
import tconv;

string[2] mod_name = ["_CONV_MODULE_","conv"];
string[] mod_hooks=[];
string[] mod_routes=["task"];

extern (C) string[] routes() {
	return mod_routes;
}
Json function(string,Json=Json.emptyObject) appFireEvent;

Tconv tc;
Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}
extern (C) int init() {
	writeln("task: Initializing!");
	tc = new Tconv(config);
	tc.start();
	return 1;
}
extern (C) string[2] name() {
	return mod_name;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}
extern (C) string[] hooks() {
	return mod_hooks;
}
extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	if(event=="_SET_TASK_"){
	}
	if(event=="_GET_TASK_"){
	}
	return j;
}

extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath=="/task"){
		try{
			Json data = Json.emptyObject;
			data.data = Json.emptyObject;
			data.data=req.json["data"];
			data["to"]="pybot";
			data["from"]=mod_name[1];
			appFireEvent("_RUN_TASKS_",data);
		}catch(Exception e){
			writeln(e.msg);
		}
	}
	res.writeBody("OK");
}

shared static this() {
//	writeln(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	writeln(mod_name[0]~": I'm being unloaded!");
	tc.stop();
	tc.join();
}
