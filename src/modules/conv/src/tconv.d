module tconv;

import std.stdio;
import std.string;
import std.datetime;
import core.thread;

import vibe.data.json;
import vibe.http.client;

class Tconv : Thread
{
    this(Json _config)
    {
    	config=_config;
    	db_url=config.global["mongo_db"].get!string;
        super( &run );
    }
    ~this(){}

public void stop(){
	go=false;
}
private:
	void runTasks(Json tasks){
		requestHTTP("http://localhost:"~config.global["port"].to!string~"/conv/task",
			(scope req) {
				req.method = HTTPMethod.POST;
				req.writeJsonBody(["data":tasks]);
			},
			(scope res) {
			}
		);
	}
	Json config;
	string db_url;
	bool go = true;
	void run(){
		writeln("Derived Convoyeur thread running.");
		this.sleep(dur!("seconds")( 5 ));
		while(go){
			Json tasks = getTasks();
			if(tasks["docs"].length>0){
				if(go){
					runTasks(tasks);
				}
				stopTasks();
			}
			this.sleep(dur!("seconds")( 20 ));
		}
	}
	void stopTasks(){
		Json data = Json.emptyObject;
		Json value = Json.emptyObject;
		Json time = Json.emptyObject;
		time["$lt"]=Clock.currTime().toUnixTime().to!int;
		value.status="active";
		value.stop=time;

		data.from="conv";
		data.key="";
		data.value=value.toString();
		data.collection="tasks";
		data.upkey="status";
		data.upval="stopped";
		try{
			requestHTTP(db_url,
				(scope req) {
					req.method = HTTPMethod.POST;
					Json jbody=Json.emptyObject;
					jbody.event="_UPDATE_RANGE_DB_";
					jbody.data=data;
					req.writeJsonBody(jbody);
				},
				(scope res) {
				}
			);
		}catch{
			writeln("DB is offline");
		}
	}
	Json getTasks(){
		Json reply = Json.emptyObject;
		Json data = Json.emptyObject;
		Json value = Json.emptyObject;
		Json time = Json.emptyObject;
		time["$lt"]=Clock.currTime().toUnixTime().to!int;
		value.status="active";
		value.time=time;

		data.from="conv";
		data.key="";
		data.value=value.toString();
		data.collection="tasks";

		try{
			requestHTTP(db_url,
				(scope req) {
					req.method = HTTPMethod.POST;
					Json jbody=Json.emptyObject;
					jbody.event="_READ_DB_";
					jbody.data=data;
					req.writeJsonBody(jbody);
				},
				(scope res) {
					reply=res.readJson();
				}
			);
		}catch{
			reply.res="FAIL";
			reply.message="DB is offline";
		}
		//updating task status
		if(reply["docs"].length>0){
			data.upkey="status";
			data.upval="halt";
			try{
				requestHTTP(db_url,
					(scope req) {
						req.method = HTTPMethod.POST;
						Json jbody=Json.emptyObject;
						jbody.event="_UPDATE_RANGE_DB_";
						jbody.data=data;
						req.writeJsonBody(jbody);
					},
					(scope res) {}
				);
			}catch{
				reply.res="FAIL";
				reply.message="DB is offline";
			}
		}
		return reply;
	}
}