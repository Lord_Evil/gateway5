import vibe.d;
import vibe.utils.array;
import std.stdio;
import std.random;
import std.regex;
import std.datetime : Clock, stdTimeToUnixTime;

string[2] mod_name = ["_WEB_","web"];
string[] mod_hooks=["_PUSH_MESSAGE_", "_BOT_CORE_MESSAGE_"];
string[] mod_routes=["","ws","css/*","js/*","files/*","img/*","plugins/*","fonts/*", "logout"];
Json function(string,Json=Json.emptyObject) appFireEvent;

WebSocket[] m_socks;

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) int init() {
	logInfo(mod_name[1]~": Initializing!");
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) string[] routes() {
	return mod_routes;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) string[] hooks() {
	return mod_hooks;
}

extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	//TODO: if user is not logged in, then do not send him any other messages apart from login
	if(event=="_PUSH_MESSAGE_"){
		foreach(WebSocket sock; m_socks){
			sock.send(data.toString());
		}
	}
	if(event=="_BOT_CORE_MESSAGE_"){
		foreach(WebSocket sock; m_socks){
			sock.send(data.toString());
		}
	}
	return j;
}


bool checkSession(HTTPServerResponse res, HTTPServerRequest req)
{
		if(!config.global.login.get!bool) return true;//let them pass
		if (!req.session || !req.session.get!bool("auth")){ 
			return false;
		}

		long activeT = req.session.get!long("active");
		long currentT = Clock.currTime().toUnixTime();
		if(activeT+config.global["timeout"].get!long<currentT){
			res.terminateSession();
			return false;
		}
		req.session.set("active",Clock.currTime().toUnixTime());
		return true;
}
bool checkSession(HTTPServerRequest req)
{
		if(!config.global.login.get!bool) return true;//let them pass
		if (!req.session || !req.session.get!bool("auth")){ 
			return false;
		}else{
			return true;
		}
}
string authPath(){
	Json data=Json.emptyObject;
	//data.data=Json.emptyObject;
	data.from=mod_name[1];
	data["to"]="auth";
	Json rep=appFireEvent("_AUTH_",data);
	return (rep.msg.type!=Json.Type.undefined)?(rep.msg.get!string):"/auth/login";
}
extern (C) void getRequest(HTTPServerResponse res, HTTPServerRequest req)
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	auto settings=new HTTPFileServerSettings();
	settings.maxAge=0.seconds;
	//logInfo(rpath);
	switch(rpath){
		case "/":
			if (!checkSession(res,req)) res.redirect(authPath()~"?r="~req.path);
			else{
				res.headers["Pragma"]="no-cache";
				serveStaticFile("./assets/"~mod_name[1]~"/public_html/index.html",settings)(req, res);
				//serveStaticFile("./assets/"~mod_name[1]~"/public_html/index.html")(req, res);
			}
			break;
		case "/logout":
			if (!checkSession(res,req)) res.redirect(authPath()~"?r="~req.path);
			else
				res.terminateSession();
				res.redirect(authPath()~"?r=/web/");
			break;
		case "/ws":
			handleWebSockets(&handleConn)(req,res);
			break;
		default:
			if(rpath.indexOf("/css/")>-1 ||rpath.indexOf("/js/")>-1||rpath.indexOf("/img/")>-1||rpath.indexOf("/files/")>-1||rpath.indexOf("/plugins/")>-1||rpath.indexOf("/fonts/")>-1){
			//	logInfo("./assets/"~mod_name[1]~"/public_html"~rpath);
				serveStaticFile("./assets/"~mod_name[1]~"/public_html/"~rpath)(req, res);
			}else{
				res.writeBody("Boo!");
			}
			break;
	}
}
void handleConn(scope WebSocket sock)
{
	logInfo("Incomming connection! "~sock.request.clientAddress.to!string~" "~sock.request.headers["Sec-WebSocket-Key"]);
	if (!checkSession(cast(HTTPServerRequest)sock.request)){
		sock.send("Must login first!");
		sock=null;
		return;
	}
	//logInfo(sock.request.headers);
	m_socks~=sock;
	while (sock.waitForData()) {
		auto msg = sock.receiveText();
		//sock.send("Shut up and listen!");
		if(msg!="keep-alive"){
			Json data=Json.emptyObject;
			data.from=mod_name[1];
			data.msg=msg;
			string[] clients;
			foreach(WebSocket s_sock;m_socks){
					clients~=(s_sock==sock?"You: ":"")~s_sock.request.clientAddress.to!string;
					s_sock.send(data.toString());
			}
			data.clients=clients.join(", ");
			sock.send(data.toString());
		}
	}
	logInfo("Connection closed! "~sock.request.headers["Sec-WebSocket-Key"]);

	m_socks.removeFromArray(sock);
	sock=null;
}

extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) //for some reason params come in reverse order when called from kernel
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
}
shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
}
