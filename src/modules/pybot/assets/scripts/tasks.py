# -*- coding: UTF-8 -*-
import json
import datetime
toJson=json.JSONEncoder()

def sendMessage(a,b):
	1

def insertDB(collection,key,value):
	1
def readOneDB(collection,key,value):
	1
def readDB(collection,key,value):
	1

def writeDB(collection,key,value,upkey,upval):
	1
def deleteDB(collection,key,value):
	1


def stopTask(_id):
	writeDB("tasks","id",_id,"status","stopped")
def delTask(_id):
	try:
		deleteDB("tasks","id",_id)
	except:
		print("Can not delete task:\n\t"+_id)

def addTask(task):
	insertDB("tasks","",task)

def listTasks(userID):
	return readDB("tasks","data.userID",userID)

def activateTask(_id,m_time):
	#print(datetime.datetime.fromtimestamp(m_time).strftime('%Y-%m-%d %H:%M:%S'))
	writeDB("tasks","id",_id,"",toJson.encode({"status":"active","time":m_time}))

def runTask(task):
	try:
		task=json.loads(task)
		if(task["data"]["type"]=="notify"):
			sendMessage(task["data"]["userID"],task["data"]["message"])
			m_time=int(task["time"])
			m_range=task["data"]["range"]
			if(m_range>0):
				m_time=int(m_time+60*60*m_range);
				activateTask(task["id"],m_time)
			else:
				stopTask(task["id"])
	except:
		print("Bad db record:\n\t"+str(task))
