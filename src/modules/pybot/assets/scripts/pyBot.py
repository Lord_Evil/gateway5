# -*- coding: UTF-8 -*-
from random import randint
import subprocess
import json
import sys

toJson=json.JSONEncoder()

def sendMessage(a,b):
	1
def getContacts():
	1
def delContacts():
	1

def insertDB(collection,key,value):
	1
def readOneDB(collection,key,value):
	1
def readDB(collection,key,value):
	1

def writeDB(collection,key,value,upkey,upval):
	1
def deleteDB(collection,key,value):
	1

def notifyOperator(event,json_data):
	1

def getLocalContacts():
	query=readDB("client","","")
	user = ""
	lang = ""
	choice = {} #на какие подписки человек согласен
	result=[]
	if(query!=None):
		contacts = json.loads(query)
		#print(query)
		for doc in contacts["docs"]:
			m_user= User()
			try:
				m_user.uid=doc["social"]["telegram"]["id"]
			except:
				print("wtf?!")
			try:
				m_user.setLang(doc["language"])
			except:
				continue
			try:
				m_user.choice=doc["ptype"]
			except:
				m_user.choice={}
			result.append(m_user)
		return result
####FLAGS
NOTVOTED=0
VOTED=1
CONFIRMING=2
CANCELED=3

class User:
	uid=""
	flag=NOTVOTED
	__lang=""
	choice=[]
	__message=""
	lastMsg=""
#	__options=[]
	ready=False
	name=""

	def __init__(self):
		return None
	def __del__(self):
		return None
	def setLang(self,lang):
		self.__lang=lang
	def getLang(self):
		return self.__lang
	def setMessage(self,msg):
		self.__message=msg
	def getMessage(self):
		return self.__message

class Poll:
	started = False
	contacts = []
	contactsSpam=[]
	pname=""
	ptype=""
	settings={}
	sysLang="ua"


	###SPAM
	def startSpam(self):
		#print("starting spam")
		for contact in self.contactsSpam:
			if(contact.ready is True):
				message=u""+contact.getMessage()
				sendMessage(contact.uid,message)
	def prepareSpam(self,data):
		if(len(self.contactsSpam)>0):
			for contact in self.contactsSpam:
				del contact
			self.contactsSpam=[]
		
		self.contactsSpam = getLocalContacts()
		self.pname=data["name"]
		self.ptype=data["ptype"]
		self.settings=data["settings"]
		if(len(self.contactsSpam)>0):
			for i in range(0,len(self.contactsSpam)): #TODO: переписать этот пиздец на foreach
				try:
					if(data["ptype"] in self.contactsSpam[i].choice.keys()): #проверка выбраных категорий
						if(self.contactsSpam[i].choice[data["ptype"]]==u"True"):
							#print(self.contactsSpam[i].uid+" wants "+data["ptype"])
							self.contactsSpam[i].setMessage(data["settings"][self.contactsSpam[i].getLang()]["info"])
							self.contactsSpam[i].ready=True
						else:
							#print(self.contactsSpam[i].uid+" doesn't want "+data["ptype"])
							continue
					else:
						#print(self.contactsSpam[i].uid+" doesn't know about "+data["ptype"])
						continue
				except (NameError,KeyError):
					print(self.contactsSpam[i].uid+" doesn't know about "+data["ptype"])
					print("Or "+self.contactsSpam[i].uid+" language is not in the system "+self.contactsSpam[i].getLang())
					continue

	def __init__(self):
		return None
#############################################FUN STARTS HERE!!!#####################################
m_poll = Poll()


def parse(data):
	try:
		data=json.loads(data)
		message=data["data"]
		peer=data["peer"]
		uid=data["userID"]
	except:
		print("Pybot faild parsing: "+data)

	if(m_poll.started):
		#m_poll.vote(message,peer)
		message=message.lower()
		contact=m_poll.getContact(uid)
		if(contact is not None):
			if(contact.ready):
				if(contact.flag==NOTVOTED):
					contact.lastMsg=message
					opts=m_poll.settings[contact.getLang()]["options"]
					try:
						opts.index(message)
						contact.setVote(message,True,m_poll.pname)
						#sendMessage(uid,m_poll.locale[contact.getLang()]["thx"])
						return
					except:
						contact.flag=CONFIRMING
						contact.name=peer
						opts=m_poll.settings[m_poll.sysLang]["options"]
						notifyOperator("VOTE_OTHER",toJson.encode({"uid":contact.uid,"name":peer,"msg":message,"opts":opts}))
						return

				if(contact.flag==CONFIRMING):
					return
				return

################################POLL CONTROLL#####################
def pollControll(data):
	#print(data)
	try:
		data=json.loads(data)
		try:
			params=data["params"]
		except:
			pass
		if(data["switch"]=="start"):
			if(params["poll"]["type"]=="spam"):
				m_poll.prepareSpam(params["poll"])
				m_poll.startSpam()
			return '{"res":"STARTED"}'
	except:
		print(str(sys.exc_info()[0])+str(sys.exc_info()[1]))
		return '{"res":"FAILED","message":"Bad json"}'
