# -*- coding: UTF-8 -*-

import sys
import json
#key:value are used for quering database as match key=value
#TODO: extend queries for different cases
toJson=json.JSONEncoder()
sysLang="ua"

def insertDB(collection,key,value):
	1
def readOneDB(collection,key,value):
	1
def readDB(collection,key,value):
	1
#firstname,lastname,middlename,dob,sex,avatar,phone,social[],email,address,comments

def writeDB(collection,key,value,upkey,upval):
	1
def deleteDB(collection,key,value):
	1

def takeAction(data):
	try:
		data=json.loads(data)
		data_from=data["from"]
		data=data["data"]
		#print(data)
	except:
		return '{"res":"FAIL","message":"JSON pooped it\'s pants"}'
	#print("Action: "+data['switch'])
	try:
		params=data["params"]
	except:
		pass

#####NET MODS STUFF
	if(data['switch']=="LIST_NET_MOD"):
		 alist=json.loads(readDB("netmods","",""))
		 try:
		 	for item in alist["docs"]:
		 		item.pop("_id")
		 	return toJson.encode(alist)
		 except:
		 	return '{}'
#####ADMIN STUFF
	if(data['switch']=="LIST_ADMIN"):
		 alist=json.loads(readDB("admin","",""))
		 try:
		 	for item in alist["docs"]:
		 		item.pop("_id")
		 	return toJson.encode(alist)
		 except:
		 	return '{}'
#####CLIENT PROFILE STUFF
	if(data['switch']=="GET_SOCIALS"):
		collection="client"
		try:
			key="chatneyID"
			value=params["chatneyID"]
		except:
			return '{"res":"FAIL","message":"missing filter: \\"key:value\\""}'
		dbdata=readOneDB(collection,key,value)
		try:
			dbdata2=json.loads(dbdata)
			dbdata=toJson.encode(dbdata2["social"])
		except:
			print(str(sys.exc_info()[0])+str(sys.exc_info()[1]))
		return dbdata

	if(data['switch']=="GET_PROFILE"):
		collection="client"
		try:
			key="social."+params["social"]+".id"
			value=params["userid"]
		except:
			return '{"res":"FAIL","message":"missing filter: \\"key:value\\""}'
		dbdata=readOneDB(collection,key,value)
		if(data_from=="api"):
			try:
				dbdata2=json.loads(dbdata)
				if("cc" in dbdata2.keys()):
					if("number" in dbdata2["cc"].keys()):
						dbdata2["cc"]["number"]="***************"
					if("code" in dbdata2["cc"].keys()):
						dbdata2["cc"]["code"]="***"
					dbdata=toJson.encode(dbdata2)
			except:
				print(str(sys.exc_info()[0])+str(sys.exc_info()[1]))
		return dbdata

	if(data['switch']=="UPDATE_PROFILE"):
		collection="client"
		try:
			key="social."+params["social"]+".id"
			value=params["userid"]
			upkey=params["upkey"]
			upval=params["upval"]
		except:
			return '{"res":"FAIL","message":"missing filter: \\"key:value\\" or data to be inserted \\"upkey:upval\\""}'
		writeDB(collection,key,value,upkey,upval)
		return '{"res":"UPDATED"}'

	if(data['switch']=="DELETE_PROFILE"):
		collection="client"
		try:
			key="social."+params["social"]+".id"
			value=params["userid"]
		except:
			return '{"res":"FAIL","message":"missing \\"name\\" value"}'
		testIfExists=readOneDB(collection,key,value)
		if(testIfExists!='"{}"'):
			deleteDB(collection,key,value)
			return '{"res":"DELETED"}'
		else:
			return '{"res":"NOTHING_TO_DELETE"}'

#####POLL STUFF
	if(data['switch']=="GET_ALL_POLL"):
		collection="poll"
		try:
			key=params["key"]
			value=params["value"]
		except:
			key=""
			value=""
		return readDB(collection,key,value)
	if(data['switch']=="GET_POLL"):
		collection="poll"
		try:
			key=params["key"]
			value=params["value"]
			return readOneDB(collection,key,value)
		except:
			return '{"res":"FAIL","message":"missing filter: \\"key:value\\""}'
	if(data['switch']=="UPDATE_POLL"):
		collection="poll"
		try:
			key="name"
			value=params["name"]
			try:
				upkey=params["upkey"]
				upval=params["upval"]
				writeDB(collection,key,value,upkey,upval)
				return '{"res":"UPDATED"}'
			except:
				upkey=""
				upval=params["value"]
				writeDB(collection,key,value,upkey,upval)
				return readDB(collection,"","")
		except:
			return '{"res":"FAIL","message":"чотоёбнулось"}'

	if(data['switch']=="NEW_POLL"):
		collection="poll"
		try:
			key=""
			value=params["value"]
			name=json.loads(value)["name"]
		except:
			return '{"res":"FAIL","message":"missing \\"name\\" value"}'
		testIfExists=readOneDB(collection,"name",name)
		#print(testIfExists)
		if(testIfExists=='{}'):
			insertDB(collection,key,value)
			return readDB(collection,"","")
		else:
			return '{"res":"EXISTS"}'

	if(data['switch']=="DELETE_POLL"):
		collection="poll"
		try:
			key="name"
			value=params["name"]
		except:
			return '{"res":"FAIL","message":"missing \\"name\\" value"}'
		testIfExists=readOneDB(collection,key,value)
		if(testIfExists!='{}'):
			deleteDB(collection,key,value)
			return '{"res":"DELETED"}'
		else:
			return '{"res":"NOTHING_TO_DELETE"}'

	if(data['switch']=="ADD_POLL_OPTS"):
		try:
			collection="poll"
			key="name"
			value=params["name"]
			upval=params["upval"]
			doc=readOneDB(collection,key,value)
		except:
			return '{"res":"FAIL","message":"Bad values"}'

		if(doc!='{}'):
			doc=json.loads(doc)
			doc["settings"][sysLang]["options"].append(upval)
			upval=doc["settings"][sysLang]["options"]
			upval=toJson.encode(upval)
			writeDB(collection,key,value,"settings."+sysLang+".options",upval)
		else:
			return '{"res":"FAIL","message":"No such a document!"}'

#####POLL TYPES STUFF
	if(data['switch']=="NEW_PTYPE"):
		collection="polltype"
		try:
			key="name"
			value=params['value']
			insertDB(collection,key,value)
			return readDB(collection,"","")
		except:
			return '{"res":"FAIL","message":"value error"}'
	if(data['switch']=="UPDATE_PTYPE"):
		collection="polltype"
		try:
			key="name"
			value=params['value']
			upval=params['upval']
			writeDB(collection,key,value,key,upval)
			return readDB(collection,"","")
		except:
			return '{"res":"FAIL","message":"value error"}'
	if(data['switch']=="DELETE_PTYPE"):
		collection="polltype"
		try:
			key="name"
			value=params['value']
			deleteDB(collection,key,value)
			return readDB(collection,"","")
		except:
			return '{"res":"FAIL","message":"value error"}'
	if(data['switch']=="LIST_PTYPE"):
		collection="polltype"
		return readDB(collection,"","")
######LANGUAGE STUFF
	if(data['switch']=="NEW_LANG"):
		collection="lang"
		try:
			key="lang"
			value=params['key']
			upkey="fullname"
			upval=params['value']
			writeDB(collection,key,value,upkey,upval)
			return readDB(collection,"","")
		except:
			return '{"res":"FAIL","message":"key:value error"}'

	if(data['switch']=="UPDATE_LANG"):
		collection="lang"
		try:
			key=params['key']
			value=params['value']
			upkey=params['upkey']
			upval=params['upval']
			writeDB(collection,key,value,upkey,upval)
			return readDB(collection,"","")
		except:
			return '{"res":"FAIL","message":"key:value error"}'

	if(data['switch']=="DELETE_LANG"):
		collection="lang"
		try:
			key=params['key']
			value=params['value']
		except:
			return '{"res":"FAIL","message":"missing \\"name\\" value"}'
		testIfExists=readOneDB(collection,key,value)
		if(testIfExists!='"{}"'):
			deleteDB(collection,key,value)
			return readDB(collection,"","")
		else:
			return '{"res":"NOTHING_TO_DELETE"}'

	if(data['switch']=="LIST_LANG"):
		collection="lang"
		try:
			return readDB(collection,"","")
		except:
			return '{"res":"FAIL"}'

	#USERS STUFF
	if(data['switch']=="LIST_USERS"):
		collection="client"
		try:
			return readDB(collection,"","")
		except:
			return '{"res":"FAIL"}'	

	#SMS STUFF
	if(data['switch']=="SAVE_MESSAGE"):
		collection=data_from
		try:
			value=data["params"]
			insertDB(collection,"", toJson.encode(value))
		except:
			return '{"res":"FAIL"}'
		return '{"res":"OK"}'

	if(data['switch']=="LIST_MESSAGES"):
		collection=data_from
		key="userID"
		val=data["params"]["userID"]
		alist=json.loads(readDB(collection,key,val))
		try:
			for item in alist["docs"]:
		 		item.pop("_id")
			return toJson.encode({"list":alist["docs"]})
		except:
			return '{}'

	if(data['switch']=="PAYMENT_PROCESSED"):
		collection="payments"
		try:
			value=data["params"]
			insertDB(collection,"", toJson.encode(value))
		except:
			return '{"res":"FAIL"}'
		return '{"res":"OK"}'


	return '{"res":"FAIL","message":"BAD_REQUEST"}'