import std.stdio;
import std.string;
import std.exception;
import std.c.stdlib;
import std.datetime;

import vibe.data.json;
import vibe.core.log;
import vibe.templ.parsertools;
import vibe.http.client;
import vibe.core.sync;

import pyd.pyd, pyd.embedded;
import deimos.python.Python;

string[2] mod_name = ["_PYBOT_","pybot"];
string[] mod_hooks=["_DB_ACTION_","_BOT_ACTION_","_RUN_TASKS_","_TASK_ACTION_"];

Json function(string,Json=Json.emptyObject) appFireEvent;
Bot m_bot;
TaskMutex botBlock;

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) int init() {
	logInfo("pybot: Initializing!");
	botBlock=new TaskMutex;
	m_bot=new Bot();
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) string[] hooks() {
	return mod_hooks;
}
extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	botBlock.lock();
	switch(event){
		case "_DB_ACTION_":
			try{
				j=m_bot.dbAction(data);
			}catch(Exception e){
				logInfo(e.msg);
			}
			break;
		case "_BOT_ACTION_":
			j=m_bot.pollControll(data["data"]);
			break;
		case "_RUN_TASKS_":
			try{
				foreach(Json task; data["data"]["docs"]){
					m_bot.runTask(task);
				}
			}catch(Exception e){
				logInfo("Failed to run tasks");
				logInfo(e.msg);
				logInfo(data.toString);
			}
			break;
		case "_TASK_ACTION_":
			try{
				data=data["data"];
				switch(data["switch"].to!string){
					case "STOP_TASK":
						m_bot.stopTask(data["params"]["taskID"].to!string);
						break;
					case "DELETE_TASK":
						m_bot.delTask(data["params"]["taskID"].to!string);
						break;
					case "ADD_TASK":
						data["params"]["id"]=Clock.currTime().toUnixTime().to!string;
						m_bot.addTask(data["params"]);
						break;
					case "LIST_TASKS":
						j=m_bot.listTasks(data["params"]["userID"].to!string);
						break;
					default:break;	
				}
			}catch(Exception e){logInfo(e.msg);}
			break;
		default:break;
	}
	botBlock.unlock();
	return j;
}

shared static this() {
	//logInfo(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
	m_bot.destroy();
	logInfo("bot is killed");
}


class Bot{
	private:
		InterpContext pyBot;

Json loopFire(string event,Json data){
	Json reply=Json.emptyObject;
	try{
		requestHTTP(config["global"]["mongo_db"].get!string,
			(scope req) {
				req.method = HTTPMethod.POST;
				Json jbody=Json.emptyObject;
				jbody.event=event;
				jbody.data=data;
				req.writeJsonBody(jbody);
			},
			(scope res) {
				reply=res.readJson();
			}
		);
	}catch{
		reply.res="FAIL";
		reply.message="DB is offline";
	}
	return reply;
}
//////////////TELEGRAM STUFF
		void sendMessage(string peer,string msg){
			//msg=dstringEscape(msg);
			//string[dchar] tTable=[' ':"_"];
			//peer=translate(peer,tTable);
			Json data = Json.emptyObject;
			data.data = Json.emptyObject;
			data.data["switch"]="SEND_MESSAGE";
			data.data.params=Json.emptyObject;
			data.data.params.message=msg;
			data.data.params.userID=peer;
			data.from=mod_name[1];
			data["to"]="telegram";
			appFireEvent("_SOCIAL_ACTION_",data);
		}
		string getContacts(){
			Json data = Json.emptyObject;
			data.from=mod_name[1];
			Json cont = appFireEvent("_GET_CONTACTS_",data);
			string[] clist;
			foreach(Json value; cont["data"]["contacts"]){
				clist~=value["name"].to!string;
			}
			return clist.join("|");
		}
		void delContacts(){
			//Json cont = m_parent.getContacts();
			//string[] clist;
			//foreach(Json value; cont["names"]){
			//	m_parent.delUser(value.to!string);
			//}
		}
//////////////DATABASE STUFF
		void insertDB(string collection, string key,string value){
			Json data = Json.emptyObject;
			data.from=mod_name[1];
			data.collection=collection;
			data.key=key;
			data.value=value;
			loopFire("_INSERT_DB_",data);
		}
		string readOneDB(string collection, string key,string value){
			Json data = Json.emptyObject;
			data.from=mod_name[1];
			data.key=key;
			data.value=value;
			data.collection=collection;
			//return appFireEvent("_READ_ONE_DB_",data).toString();
			return loopFire("_READ_ONE_DB_",data).toString();
		}
		string readDB(string collection, string key,string value){
			Json data = Json.emptyObject;
			data.from=mod_name[1];
			data.key=key;
			data.value=value;
			data.collection=collection;
			//return appFireEvent("_READ_DB_",data).toString();
			return loopFire("_READ_DB_",data).toString();
		}
		void writeDB(string collection, string key,string value,string upkey,string upvalue){
			Json data = Json.emptyObject;
			data.from=mod_name[1];
			data.collection=collection;
			data.value=value;
			data.key=key;
			try{
				data.upval=parseJsonString(upvalue);
				//logInfo("Parsed: "~data["upval"].toString());
			}catch(Exception e){
				data.upval=upvalue;
			}
			data.upkey=upkey;
			data.upval=upvalue;
			loopFire("_WRITE_DB_",data);
		}
		void deleteDB(string collection, string key,string value){
			Json data = Json.emptyObject;
			data.from=mod_name[1];
			data.collection=collection;
			data.key=key;
			data.value=value;
			loopFire("_DELETE_DB_",data);
		}
/////////BOT STUFF
		void notifyOperator(string event,string botdata){
			Json data = Json.emptyObject;
			data.from="pollBot";
			data.event=event;
			try{
				data.bot=parseJsonString(botdata);
			}catch(Exception e){
				logInfo("Bot returned bad Json:"~e.msg);
				data.bot=botdata;
			}
			appFireEvent("_POLL_MESSAGE_",data);
		}

	public:
		~this(){
			logInfo("Trying to kill bot...");
			try{
				py_finish();
			}
			catch(Exception e){
				logInfo(e.msg);
			}
		}
		this(){
			py_init();

			pyBot = new InterpContext();
			pyBot.py_stmts("import sys");
			pyBot.py_stmts("import os");
			pyBot.py_stmts("sys.path.append(os.getcwd()+'/assets/"~mod_name[1]~"/scripts/')");
			try{
				pyBot.py_stmts("import pyBot");
				pyBot.py_stmts("import db");
				pyBot.py_stmts("import tasks");

				pyBot.insertDB = py(&insertDB);
				pyBot.py_stmts("db.insertDB=insertDB");
				pyBot.py_stmts("pyBot.insertDB=insertDB");
				pyBot.py_stmts("tasks.insertDB=insertDB");

				pyBot.readDB = py(&readDB);
				pyBot.py_stmts("db.readDB=readDB");
				pyBot.py_stmts("pyBot.readDB=readDB");
				pyBot.py_stmts("tasks.readDB=readDB");
				
				pyBot.readOneDB = py(&readOneDB);
				pyBot.py_stmts("db.readOneDB=readOneDB");
				pyBot.py_stmts("pyBot.readOneDB=readOneDB");
				
				pyBot.writeDB = py(&writeDB);
				pyBot.py_stmts("db.writeDB=writeDB");
				pyBot.py_stmts("pyBot.writeDB=writeDB");
				pyBot.py_stmts("tasks.writeDB=writeDB");
				
				pyBot.deleteDB = py(&deleteDB);
				pyBot.py_stmts("db.deleteDB=deleteDB");
				pyBot.py_stmts("pyBot.deleteDB=deleteDB");
				pyBot.py_stmts("tasks.deleteDB=deleteDB");
				
				pyBot.sendMessage = py(&sendMessage);
				pyBot.py_stmts("pyBot.sendMessage=sendMessage");
				pyBot.py_stmts("tasks.sendMessage=sendMessage");

				pyBot.getContacts = py(&getContacts);
				pyBot.py_stmts("pyBot.getContacts=getContacts");
				//pyBot.delContacts = py(&delContacts);
				//pyBot.py_stmts("pyBot.delContacts=delContacts");

				pyBot.notifyOperator = py(&notifyOperator);
				pyBot.py_stmts("pyBot.notifyOperator=notifyOperator");
			}
			catch(Exception e){
				logInfo("Loading script failed: "~e.msg);
				exit(0);
			}
		}
		Json dbAction(Json data){
			
			Json reply = Json.emptyObject;
			if(data["params"].type==Json.Type.object){
				if(data["params"]["value"].type==Json.Type.object)	//если содержит контейнер типа Json, упаковываем в строку
					data["params"]["value"]=data["params"]["value"].toString();
			}
			PydObject dbReply;
			pyBot.action=data.toString();
			try{
				dbReply=pyBot.py_eval("db.takeAction(action)");
				reply=parseJsonString(dbReply.toString());
			}catch(Exception e){
				logInfo("Bot returned bad Json:"~e.msg);
				reply=dbReply.toString();
			}
			
			return reply;
		}
		Json pollControll(Json data){
			Json reply=Json.emptyObject;
			PydObject botReply;
			pyBot.data=data.toString();
			botReply=pyBot.py_eval("pyBot.pollControll(data)");
			try{
				reply=parseJsonString(botReply.toString());
			}catch(Exception e){
				logInfo("Bot returned bad Json:"~e.msg);
				reply=botReply.toString();
			}
			return reply;
		}
		void runTask(Json task){
			pyBot.data=task.toString();
			pyBot.py_stmts("tasks.runTask(data)");
		}
		void stopTask(string taskID){
			pyBot.data=taskID;
			pyBot.py_stmts("tasks.stopTask(data)");
		}
		void delTask(string taskID){
			pyBot.data=taskID;
			pyBot.py_stmts("tasks.delTask(data)");
		}
		void addTask(Json task){
			pyBot.data=task.toString();
			pyBot.py_stmts("tasks.addTask(data)");
		}
		Json listTasks(string userID){
			Json reply=Json.emptyObject;
			PydObject botReply;
			pyBot.data=userID;
			botReply=pyBot.py_eval("tasks.listTasks(data)");
			try{
				reply=parseJsonString(botReply.toString());
			}catch(Exception e){
				logInfo("Bot returned bad Json:"~e.msg);
				reply=botReply.toString();
			}
			return reply;
		}
}
