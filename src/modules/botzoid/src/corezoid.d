/**
 * Corezoid D Module
 *
 * Refubrished https://github.com/corezoid/sdk-php
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category        Corezoid
 * @package         Corezoid/Corezoid
 * @version         1.0
 * @author          corezoid.com
 * @copyright       Copyright (c) 2013 corezoid.com
 * @license         http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 *
 * EXTENSION INFORMATION
 *
 * Corezoid API       http://www.corezoid.com/how_to_use/api/en/
 *
 */

/**
 * Corezoid Class
 *
 * @author      Lord_Evil <Lord_Evil@om-webmasters.org>
 * @uses    vibe.data.json
 * @uses    vibe.http.client
 */
module corezoid;
import std.datetime;
import std.digest.sha;
import std.uni;
import vibe.data.json;
import vibe.http.client;

import std.stdio;

class Corezoid
{
    /**
     * host Corezoid
     */
    private string _host = "https://www.corezoid.com";

    /**
     * Version API
     */
    private string _version = "1";

    /**
     * Format API
     */
    private string _format = "json";

    /**
     * User API
     */
    private string _api_login;

    /**
     * API secret key
     */
    private string _api_secret;

    /**
     * Array tasks
     */
    private Json[] _tasks;
    /**
     * Constructor.
     *
     * @param string $api_login
     * @param string $api_secret
     * 
     * @throws InvalidArgumentException
     */
    public this(string api_login, string api_secret)
    {
        _api_login  = api_login;
        _api_secret = api_secret;
    }

   public Json setStatus(string conv_id, string status){
    Json data = Json.emptyObject;
    data.ops = Json.emptyObject;
    data.ops["type"] = "modify";
    data.ops.obj = "conv";
    data.ops.obj_id=conv_id;
    data.ops.status=status;

    return callAPI(data);
   }

    /**
     * Add new task
     *
     * @param string $ref
     * @param string $conv_id
     * @param array $data
     *
     */
    public void addTask(string _ref, string conv_id, Json data = Json.emptyObject)
    {
        Json task = Json.emptyObject;
        task["ref"]=_ref;
        task["type"]="create";
        task["obj"]="task";
        task["conv_id"]=conv_id;
        task["data"]=data;
        _tasks~=task;
    }


    /**
     * Send tasks to Corezoid 
     *
     * @return string
     */
    public Json sendTasks()
    {
        Json content = Json.emptyObject;
        content.ops=_tasks;
        _tasks.length=0;
        return callAPI(content);
    }

    private Json callAPI(Json content){
        Json response;
        string url = makeURL(content);
        requestHTTP(url,
            (scope req)
            {
                req.method=HTTPMethod.POST;
                req.writeBody(cast(ubyte[])content.toString,"application/json");
            },
            (scope res)
            {
                try{
                    response = res.readJson;
                }catch(Exception e){
                    writeln(e.msg);
                    writeln("Url <"~url~">");
                    writeln(res.toString());
                }
            }
        );
        return response;
    }

    /**
     * Check Signature
     *
     * @param string $sign
     * @param string $time
     * @param string $content
	 *
     * @return string
     */
    public bool checkSign(string sign, string time, Json content)
    {
    	string cmp_sign = makeSign(time, content);
    	return (cmp_sign == sign) ? true : false;
    }


    /**
     * Create URL to Corezoid
     *
     * @param string $time
     * @param string $content
     *
     * @return string
     */
    private string makeURL(Json content)
    {
        string time = Clock.currTime().toUnixTime().to!string;
    	string sign = makeSign(time, content); 

    	return (_host~"/api/"~_version~'/'~_format~'/'~_api_login~'/'~time~'/'~sign);
    }


    /**
     * Create Signature
     *
     * @param string $time
     * @param string $content
     *
     * @return string
     */
    private string makeSign(string time, Json content)
    {
        string sign=toHexString(sha1Of(time~_api_secret~content.toString~_api_secret));
//        writeln("###Debug:\nTime: "~time~"\nContent: "~content.toString~"\nString to sign: "~time~_api_secret~content.toString~_api_secret~"\nSign: "~sign.toLower);
    	return sign.toLower.dup;
    }
}
