import vibe.data.json;
import vibe.http.server;
import vibe.core.log;

import std.string;
import std.random;
import std.datetime;

import corezoid;

string[2] mod_name = ["_BOTZOID_","botzoid"];
string[] mod_hooks=["_PUSH_MESSAGE_"];

Json function(string,Json=Json.emptyObject) appFireEvent;

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) int init() {
	logInfo(mod_name[1]~": Initializing!");
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) string[] hooks() {
	return mod_hooks;
}

extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	if(event=="_PUSH_MESSAGE_"){
		if(data.data.params.way.get!string=="to"){
			Corezoid conv = new Corezoid(config.mod["api_login"].get!string,config.mod["api_secret"].get!string);
			string ref1 = Clock.currTime().toUnixTime().to!string ~'_'~uniform(500000,1000000).to!string;
			Json task = Json.emptyObject;
			task.uname=data.data.params.userID;
			task.message=data.data.params.data;
			conv.addTask(ref1,config.mod["conv_id"].get!string,task);
			Json res=conv.sendTasks();
			//logInfo(res.toString);
			delete(conv);
		}
	}
	j.from=mod_name[1];
	j.res="OK";
	return j;
}

shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
}
