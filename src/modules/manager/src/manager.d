import vibe.data.json;
import vibe.http.server;
import vibe.core.log;
import vibe.http.client;

import std.string;

string[2] mod_name = ["_SOCIAL_MANAGER_","manager"];
string[] mod_hooks=["_MANAGER_ACTION_"];
string[] mod_routes=["sendMessage","setEvent","findEvent","cancelEvent","event/fire"];

extern (C) string[] routes() {
	return mod_routes;
}

Json function(string,Json=Json.emptyObject) appFireEvent;

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) int init() {
	logInfo(mod_name[1]~": Initializing!");
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) string[] hooks() {
	return mod_hooks;
}

extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	Json params=Json.emptyObject;
	if(data["data"]["params"].type==Json.Type.object){
		params=data["data"]["params"];
	}
	try{
		switch(data["data"]["switch"].get!string){
			case "IDENTIFY": //returns it's name
				j.data=Json.emptyObject;
				j.data.name=mod_name[1];//TODO: return module name, messanger type, client ID
				break;
			default:
				j.data="BAD_REQUEST";
				break;
		}
	}catch(Exception e){
		logInfo(mod_name[0]~": "~e.msg);
	}
	j.from=mod_name[1];
	j.res="OK";
	return j;
}
string findTelegramByChatneyID(string chatneyID){
	//{"to":"pybot","data":{"switch":"GET_PROFILE","params":{"chatneyID":indata["to"].get!string}},"from":mod_name[1]}
	Json userData=Json.emptyObject;
	userData["to"]="pybot";
	userData["from"]=mod_name[1];
	userData["data"]=Json.emptyObject;
	userData["data"]["switch"]="GET_SOCIALS";
	userData["data"]["params"]=Json.emptyObject;
	userData["data"]["params"]["chatneyID"]=chatneyID;
	Json socials=appFireEvent("_DB_ACTION_",userData);
	return socials["telegram"]["id"].get!string;
}
void sendMessage(HTTPServerRequest req, HTTPServerResponse res){
	Json j=Json.emptyObject;
	try{
		Json indata=req.json;
		Json data = Json.emptyObject;
		data.data=Json.emptyObject;
		data.data["switch"]="SEND_MESSAGE";
		data.data.params=Json.emptyObject;
		if(indata["social"].get!string=="chatney"){
			data["to"]="telegram";
			try{
				data.data.params["userID"]=findTelegramByChatneyID(indata["to"].get!string);
			}catch{
				j["status"]="fail";
				//j["socials"]=socials;
				res.writeJsonBody(j);
				return;
			}
		}else{
			data.data.params["userID"]=indata["to"].get!string;
			data["to"]=indata["social"].get!string;
		}
		data.data.params["message"]=indata["text"].get!string;
		data.from=mod_name[1];
		appFireEvent("_SOCIAL_ACTION_",data);

		j.status="success";
	}catch{
		j.status="Error";
		j.msg="Expecting JSON";
		res.writeJsonBody(j);
		return;
	}
	res.writeJsonBody(j);
}

extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	switch(rpath){
		case "/sendMessage":
			try{
				sendMessage(req,res);
			}catch(Exception e){
				logInfo(e.msg);
			}
			break;
		case "/setEvent":
			logInfo("Maganager: Setting Event");
			try{
				Json event=Json.emptyObject;
				event["data"]=req.json["data"];
				event["finish"]=req.json["finish"];
				event["callerKey"]=config.mod["instance"].get!string;
				event["callback"]=config.mod["callback"].get!string;
				try{
					//TODO: url to config
					requestHTTP(config.mod["timer"].get!string~"/setEvent",
						(scope req) {
							req.method = HTTPMethod.POST;
							req.writeJsonBody(event);
						},
						(scope response) {
							res.writeJsonBody(response.readJson);
						}
					);
				}catch{
					res.writeJsonBody(["status":"deadTimer"]);
				}
				return;
			}catch{
				logInfo("Bad json!");
				res.writeJsonBody(["status":"badJson"]);
			}
			break;
		case "/findEvent":
			logInfo("Maganager: Find Event");
			try{
				Json event=Json.emptyObject;
				if(req.json["query"].type!=Json.Type.undefined)
					event["query"]=req.json["query"];
				event["callerKey"]=config.mod["instance"].get!string;
				try{
					//TODO: url to config
					requestHTTP(config.mod["timer"].get!string~"/findEvent",
						(scope req) {
							req.method = HTTPMethod.POST;
							req.writeJsonBody(event);
						},
						(scope response) {
							res.writeJsonBody(response.readJson);
						}
					);
				}catch{
					res.writeJsonBody(["status":"deadTimer"]);
				}
				return;
			}catch{
				logInfo("Bad json!");
				res.writeJsonBody(["status":"badJson"]);
			}
			break;
		case "/cancelEvent":
			logInfo("Maganager: Canceling Event");
			try{
				Json event=Json.emptyObject;
				if(req.json["idKey"].type==Json.Type.undefined){
					res.writeJsonBody(["status":"idKeyMissing"]);
					return;
				}

				event["idKey"]=req.json["idKey"];
				event["callerKey"]=config.mod["instance"].get!string;
				try{
					//TODO: url to config
					requestHTTP(config.mod["timer"].get!string~"/cancelEvent",
						(scope req) {
							req.method = HTTPMethod.POST;
							req.writeJsonBody(event);
						},
						(scope response) {
							res.writeJsonBody(response.readJson);
						}
					);
				}catch{
					res.writeJsonBody(["status":"deadTimer"]);
				}
				return;
			}catch{
				logInfo("Bad json!");
				res.writeJsonBody(["status":"badJson"]);
			}
			break;
		case "/event/fire":
			logInfo("Maganager: Firing Event");
			try{
				Json data=req.json;
				string userID;
				if(data["user"].type!=Json.Type.undefined && data["user"]["chatneyID"].type==Json.Type.string){
					try{
						userID=findTelegramByChatneyID(data["user"]["chatneyID"].get!string);
					}catch{
						logInfo("chatneyID not found in db");
					}
				}else{
					logInfo("No chatneyID in event!");
					logInfo(data.toString());
					res.writeJsonBody(["status":"OK"]);
					return;
				}
				//start botCore session if not started
				Json event=Json.emptyObject;
				event["netmod_to"]="botCore";
				event["to"]="netmod";
				event["from"]=mod_name[1];
				event["data"]=Json.emptyObject;
				event["data"]["switch"]="SESSION_START";
				event["data"]["params"]=Json.emptyObject;
				event["data"]["params"]["cmd"]="###calendar '"~data.toString()~"'";
				event["data"]["params"]["userID"]=userID;
				event["data"]["params"]["social"]="telegram";
				Json botStatus=appFireEvent("_BOT_CORE_",event);
				logInfo(botStatus.toString());
				//if(!isset($res["data"]["data"])||!isset($res["data"]["data"]["rep"])) return ["status"=>"fail"];
				if(botStatus["data"].type!=Json.Type.undefined&&botStatus["data"]["data"].type!=Json.Type.undefined&&botStatus["data"]["data"]["rep"].type==Json.Type.string){
					if(botStatus["data"]["data"]["rep"].get!string=="STARTED"){
						//logInfo("Session started. Send more data");
						logInfo("Session started.");

					}else if(botStatus["data"]["data"]["rep"].get!string=="RUNNING"){
						logInfo("Session is already running. Send to queue.");
						event["data"]["switch"]="LAUNCH_FSM";
						event["data"]["params"]["machine"]=Json.emptyObject;
						event["data"]["params"]["machine"]["namespace"]="AppSelector";
						event["data"]["params"]["machine"]["priority"]="100";
						event["data"]["params"].remove("cmd");
						event["data"]["params"]["data"]=Json.emptyObject;
						event["data"]["params"]["data"]["text"]="###calendar '"~data.toString()~"'";
						botStatus=appFireEvent("_BOT_CORE_",event);
					}
				}else{
					logInfo("botCore is dead");
				}

				res.writeJsonBody(["status":"OK"]);
			}catch{
				logInfo("Bad json from timer!");
				res.writeJsonBody(["status":"OK"]);
			}
			break;
		default:
			res.writeJsonBody(["status":"OK"]);
			break;
	}
}

extern (C) void getRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	res.writeJsonBody(["status":"OK"]);
}
shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
}
