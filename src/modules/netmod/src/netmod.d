import vibe.data.json;
import vibe.http.server;
import vibe.http.client;
import vibe.core.log;

import std.algorithm;
import std.stdio;
import std.string;
import std.datetime : Clock, stdTimeToUnixTime;
import std.random;
import std.digest.sha;

string[2] mod_name = ["_NETMOD_","netmod"];
string[] mod_routes=["hello/:mod", "reg/:mod/:sign", "addsubs/:mod/:sign","rmsub/:mod/:sign","end/:mod/:sign","push/:mod/:sign"];
Json function(string,Json=Json.emptyObject) appFireEvent;
string[] mod_hooks;

extern (C) string[] hooks() {
	return mod_hooks;
}
Json config;

extern (C) void setConfig(Json c){
	config = Json.emptyObject;
	config.global=c.global;
	config.mod=c.mod;
}

extern (C) int init() {
	logInfo(mod_name[1]~": Initializing!");
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) string[] routes() {
	return mod_routes;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

struct RemoteMod
{
	Json[] subs; //{"subcription":{"rights":["op","doctor","admin"],"callback":"http://servername.com/callback"}}
	string name;
	long seq;
	long regSeq;
	string key;
}
RemoteMod[string] remMods;
Json [string][string] subscriptions; // [sub:[mod:{callback:"",rights:[]}]]

extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	if(data["netmod_to"].to!string != "undefined"){
		try{
			j["data"]=callBack(subscriptions[event][data["netmod_to"].get!string]["callback"].get!string,event,data);
			j["data"]["netmod_from"]=data["netmod_to"];
		}catch(Exception e){
			j.rep="NO SUBSCRIBER";
		}
	}else{
		try{
			j.list = Json.emptyArray;
			foreach(string key, Json mod;subscriptions[event]){
				Json callData = Json.emptyObject;
				callData.netmod_from=key;
				callData["data"]=Json.emptyObject;
				try{
					callData["data"]=callBack(mod["callback"].get!string,event,data);
				}catch(Exception e){
					logInfo("Netmod - calling "~key~": "~e.msg~"\nRemoving "~key~" subscriber");
					subscriptions[event].remove(key);
				}
				j.list~=callData;
			}
		}catch(Exception e){logInfo(e.msg);}
	}
	j.from=mod_name[1];
	return j;
}
Json callBack(string url, string event, Json data){
	Json j = Json.emptyObject;
	requestHTTP(url,
		(scope req) {
			req.method = HTTPMethod.POST;
			//req.headers["Connection"]="Close";
			Json jbody = Json.emptyObject;
			jbody.data=data;
			jbody.event=event;
			req.writeJsonBody(jbody);
		},
		(scope res) {
			try{
				j=res.readJson();
			}catch(Exception e){
				logInfo("Netmod - readJson(): "~e.msg);
			}
		}
	);
	return j;
}
long genSeq(){
	return uniform(100000000000000, 999999999999999);
}
bool checkSign(string mod_name, string sign, Json* j){
	try{
		RemoteMod mod = remMods[mod_name];
		if(sign==toHexString(sha1Of(mod.seq.to!string~mod.key~mod.seq.to!string))){
			long seq = genSeq();
			mod.seq=seq;
			remMods[mod_name]=mod;
			j.seq=seq;
			return true;
		}else{
			return false;
		}
	}catch{
		return false;
	}
}
extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) //for some reason params come in reverse order when called from kernel
{
	string rpath = req.path[req.path.indexOf("/",1)+1..$].split("/")[0];
	string net_mod_name = req.params["mod"];
	Json j = Json.emptyObject;
	switch(rpath){
		case "addsubs":
			string sign = req.params["sign"];
			try{
				if(checkSign(net_mod_name, sign, &j)){
					Json data = req.json;
					foreach(string key, Json sub; data){
						if(countUntil(mod_hooks,key)<0) mod_hooks~=key;
						subscriptions[key][net_mod_name]=Json.emptyObject;
						subscriptions[key][net_mod_name].callback=sub["callback"];
						subscriptions[key][net_mod_name].rights=sub["rights"];
					}
					j.rep="SUBSCRIPTIONS ADDED";
				}
			}catch(Exception e){logInfo(e.msg);}
			break;
		case "rmsub":
			string sign = req.params["sign"];
			try{
				if(checkSign(net_mod_name, sign, &j)){
					Json data=req.json;
					subscriptions[data["name"].get!string].remove(net_mod_name);
					j.rep="SUBSCRIPTION REMOVED";
				}
			}catch{}
			break;
		case "push":
			string sign = req.params["sign"];
			try{
				if(checkSign(net_mod_name, sign, &j)){
					string event = req.json["event"].get!string;
					Json data = Json.emptyObject;
					data.data = req.json["data"];
					data.from=mod_name[1];
					data.netmod_from=net_mod_name;
					if(req.json["to"].get!string != "undefined")
						data["to"]=req.json["to"].get!string;
					j["data"]=appFireEvent(event,data);
					j.rep="OK";
				}else{
					j.rep="BAD SIGN";
				}
			}catch(Exception e){logInfo(e.msg);}
			break;
		default:break;
	}
	res.writeJsonBody(j);
}

extern (C) void getRequest(HTTPServerResponse res, HTTPServerRequest req) //for some reason params come in reverse order when called from kernel
{
	Json j = Json.emptyObject;
	string rpath = req.path[req.path.indexOf("/",1)+1..$].split("/")[0];
	string net_mod_name = req.params["mod"];
	switch(rpath){
		case "hello":
			Json data = Json.emptyObject;
			data.data = Json.emptyObject;
			data.data["switch"] = "LIST_NET_MOD";
			data.from = mod_name[1];
			data["to"] = "pybot";
			Json mods = appFireEvent("_DB_ACTION_",data);
			if(mods.length==0) break;
			foreach(Json item; mods["docs"]){
				if(item["name"].get!string==net_mod_name){
					RemoteMod mod;
					long seq = genSeq();
					mod.regSeq=seq;
					mod.name=net_mod_name;
					mod.key=item["key"].to!string;
					remMods[net_mod_name]=mod;
					j.rep="OK";
					j.seq=seq;
				}
			}
			break;
		case "reg":
			string sign = req.params["sign"];
			try{
				RemoteMod mod = remMods[net_mod_name];
				if(sign==toHexString(sha1Of(mod.regSeq.to!string~mod.key~mod.regSeq.to!string))){
					mod.regSeq = genSeq();//so that we don't leave it the same or 0
					long seq = genSeq();
					mod.seq=seq;
					remMods[net_mod_name]=mod;
					j.rep="SESSION STARTED";
					j.seq=seq;
				}
			}catch{}
			break;
		case "end":
			string sign = req.params["sign"];
			try{
				RemoteMod mod = remMods[net_mod_name];
				Json jnull = Json.emptyObject;
				if(checkSign(net_mod_name, sign, &jnull)){
					remMods.remove(net_mod_name);
					//TODO: remove from subscription list
					j.rep="SESSION ENDED";
				}
			}catch{}
			break;
		default:break;
	}
	res.writeJsonBody(j);
}
shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
	}


shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
}
