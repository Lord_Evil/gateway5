import vibe.data.json;
import vibe.core.log;

import std.stdio;
import std.string;

private import tcom;

Json parseMsg(string item){
	Json msg=Json.emptyObject;
	bool secret=false;
	try{
		msg.secretID=item[item.indexOf("secret#")..(item.indexOf("]",item.indexOf("secret#")))];
		secret=true;
	}catch{ }
	try{
		msg.userID=item[item.indexOf("user#")..(item.indexOf("]",item.indexOf("user#")))];
	}catch{ }
	try{
		if(item.indexOf(">>>")>0 || item.indexOf("»»»")>0){
			msg.way="to";
			if(item.indexOf("»»»")>0){
				try{
					msg.peer=item[(item.indexOf("]",item.indexOf("user#"))+1)..(item.indexOf("»")-1)].to!string.stripLeft();
					msg.data=item[(item.indexOf("»")+6)..(item.length)].to!string.stripRight();
				}catch(Exception e){
					logInfo("Fail in line: "~item);
				}
			}else{
				try{
					msg.peer=item[(item.indexOf("]",item.indexOf("user#"))+1)..(item.indexOf(">")-1)].to!string.stripLeft();
					msg.data=item[(item.indexOf(">")+4)..(item.length)].to!string.stripRight();
				}catch(Exception e){
					logInfo("Fail in line: "~item);
				}
			}
		}else{
			msg.way="from";
			if(item.indexOf("«««")>0){
				try{
					if(secret){
						msg.peer=item[(item.indexOf("]",item.indexOf("secret#"))+1)..(item.indexOf("«")-1)].to!string.stripLeft();
					}else{
						msg.peer=item[(item.indexOf("]",item.indexOf("user#"))+1)..(item.indexOf("«")-1)].to!string.stripLeft();
					}
					msg.data=item[(item.indexOf("«")+6)..(item.length)].to!string.stripRight().stripLeft();
				}catch(Exception e){
					logInfo("Fail in line: "~item);
				}
			}else{
				try{
					if(secret){
						msg.peer=item[(item.indexOf("]",item.indexOf("secret#"))+1)..(item.indexOf("<")-1)].to!string.stripLeft();
					}else{
						msg.peer=item[(item.indexOf("]",item.indexOf("user#"))+1)..(item.indexOf("<")-1)].to!string.stripLeft();
					}
					msg.data=item[(item.indexOf("<")+4)..(item.length)].to!string.stripRight().stripLeft();
				}catch(Exception e){
					logInfo("Fail in line: "~item);
				}
			}
		}
		if(secret){
			string[dchar] tTable=['_':" "];
			msg.peer=translate((msg.peer.get!string)[2..$],tTable);
		}		
		msg.id=item[0..(item.indexOf(" "))].to!long;
		if(item.indexOf("[photo]")>1){
			msg["type"]="photo";
		}else if(item.indexOf("[document")>1){
			msg["type"]="document";
		}else if(item.indexOf("[geo]")>1){
			msg.data=item[(item.indexOf("q=")+2)..(item.length)].stripRight();
			msg["type"]="geo";
		}else if(item.indexOf("[audio:")>1){
			msg["type"]="audio";
		}else{
			msg["type"]="text";
		}
		msg.time=item[(item.indexOf("[")+1)..item.indexOf("]")].to!string;
	}catch(Exception e){
		logInfo("Parsing message failed: "~e.msg);
		logInfo(item);
	}

		return msg;
}

string stripControl(string text){
	string outputString;
	bool inEscape = false;
	bool justEnteredEscape = false;
	foreach(c; text) {
		if(justEnteredEscape) {
			justEnteredEscape = false;
			if(c == '['){
				inEscape = true;
			}else {
				outputString ~= c;
       		}
       	}else if(inEscape) {
			if(c == 'm') inEscape = false;
		} else if(c == '\033') {
			justEnteredEscape = true;
		} else {
			if(c == 8) continue;
			if(c == 0) continue;
			outputString ~= c;
		}

	}
	return outputString;
}
