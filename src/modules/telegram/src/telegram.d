import vibe.d;
import vibe.data.json;
import vibe.utils.array;
import std.stdio;
import std.variant;

private import tthread;
private import textops;
private import tcom;

string[2] mod_name = ["_TELEGRAM_","telegram"];
string[] mod_hooks=["_SOCIAL_ACTION_"];
string[] mod_routes=["push","save","file/:msgID/:type/:fileName"];
//TODO: process more events

extern (C) string[] routes() {
	return mod_routes;
}

Json function(string,Json=Json.emptyObject) appFireEvent;
TelegramThread tt;
Tcom com;

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) int init() {
	logInfo("telegram: Initializing!");
	int port =config["global"]["port"].get!int;
	tt = new TelegramThread(port);
	tt.start();
	com = new Tcom(config);
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) string[] hooks() {
	return mod_hooks;
}
extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	com.client();
	Json j=Json.emptyObject;
	Json params=Json.emptyObject;
	if(data["data"]["params"].type==Json.Type.object){
		params=data["data"]["params"];
	}
	try{
		switch(data["data"]["switch"].get!string){
			case "IDENTIFY": //returns it's name
				j.data=Json.emptyObject;
				j.data.name=mod_name[1];//TODO: return module name, messanger type, client ID
				break;
			case "GET_CONTACTS": //returns list of contacts
				j.data=com.getContacts();
				break;
			case "GET_DIALOGS": //returns list of dialogs and unread messages
				j.data=com.getDialogs();
				break;
			case "GET_MESSAGES": //returns last ten messages. TODO: add option to load dialog tops (more messages)
				j.data=com.getMessages(params["userID"].get!string);
				break;
			case "GET_LOCAL_MESSAGES":
				j.data=getMessages(params["userID"].get!string);
				break;
			case "SEND_MESSAGE": //sends message. TODO: images, files, documents, maps
				com.sendMessage(params["userID"].get!string,params["message"].get!string);
				break;
			case "GET_FILE":
				j.data=Json.emptyObject;
				j.data.base64=com.getBase64File(params["msgID"].get!string,params["type"].get!string);
				//j.data.base64="data:image/png;base64,PNG...";
				writeln(j);
				break;
			case "ADD_CONTACT":
				j.data=com.addContact(params["userID"].get!string,params["fname"].get!string,params["lname"].get!string);
				break;
			//TODO: update contact, delete contact
			default:
				j.data="BAD_REQUEST";
				break;
		}
		logInfo("#################TELEGRAM EVENT IN");
		logInfo("R: "~data["data"].toString());
		logInfo("A: "~j.data.toString());
		logInfo("################END TELEGRAM EVENT");
	}catch(Exception e){
		logInfo("#################TELEGRAM EVENT IN");
		logInfo("R: "~data["data"].toString());
		logInfo("Telegram command failed!");
		logInfo("A: "~e.msg);
		logInfo("################END TELEGRAM EVENT");
	}
	j.from=mod_name[1];
	j.res="OK";
	return j;
}

Json getMessages(string userid){
	Json data = Json.emptyObject;
	data.data=Json.emptyObject;
	data.data["switch"]="LIST_MESSAGES";
	data.data.params=Json.emptyObject;
	data.data.params["userID"]=userid;
	data["to"]="pybot";
	data["from"]=mod_name[1];
	return appFireEvent("_DB_ACTION_",data);
}

extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath=="/push"){
		//res.headers["Access-Control-Allow-Origin"]="*";
		try{
			appFireEvent("_PUSH_MESSAGE_",req.json["data"]);
		}catch(Exception e){
			logInfo(e.msg);
		}
		
	}
	if(rpath=="/save"){
		//res.headers["Access-Control-Allow-Origin"]="*";
		try{
			appFireEvent("_DB_ACTION_",req.json["data"]);
		}catch(Exception e){
			logInfo(e.msg);
		}
	}
	res.writeBody("OK");
}
extern (C) void getRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	com.client();
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath.indexOf("/file")>-1){
		try{
			Variant [string]file=com.getFile(req.params["msgID"],req.params["type"]);
			if(file.length==2){
				res.writeBody(file["file"].get!(ubyte[]),file["ftype"].get!(string));
			}else{
				res.writeBody("");
			}
		}catch(Exception e){
			res.writeBody(e.msg);
		}
	}else{
		res.writeBody("OK");
	}
}

shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
	//tt.stop();
	tt.join();
}
