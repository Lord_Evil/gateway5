module tcom;
import vibe.data.json;
import vibe.core.log;
import vibe.http.websockets;
import vibe.core.sync;

import magic;

import std.stdio;
import std.string;
import std.socket;
import std.c.stdlib;
import std.datetime;
import std.base64;
import std.file;
import std.variant;

import textops;

//import std.regex;

class Tcom
{
	private Socket m_sock;
	private TaskMutex sockBlock;
	private WebSocket[] w_socks;
	private Json config;

	public void setSock(WebSocket[] socks){
		w_socks=socks;
	}
	public Json getContacts(){
		Json reply = Json.emptyObject;
		Json[] contacts;
		string[] list;
		try{
			list=commSock("contact_list\n").splitLines;
		}catch(Exception e){
			logInfo("Ёбнулось при запросе списка контактов! "~e.msg);
		}
		try{
			foreach(string item;list){
				Json contact=Json.emptyObject;
				contact.userID=item[1..(item.indexOf("]"))];
				contact.name=item[(item.indexOf("]")+1)..item.length].stripRight();
				contacts~=contact;
			}
		}catch(Exception e){
			logInfo("Ебнулось при чтении списка! "~e.msg);
			logInfo("Такой вот списочек:\n"~list.join("\n"));
		}
		reply.list=contacts;
		return reply;
	}
	public Json getDialogs(){
		Json reply = Json.emptyObject;
		Json[] dialogs;
		string[] list;
		try{
			list=commSock("dialog_list\n").splitLines;
		}catch(Exception e){
			logInfo("Ёбнулось при запросе списка диалогов! "~e.msg);
		}

		try{
			foreach(string item;list){
				if(item.indexOf("User")>-1 && item.indexOf("unread")>-1 && item.indexOf("User")<item.indexOf("unread")){
					Json chat=Json.emptyObject;
					chat.userID=item[6..item.indexOf("]")];
					chat.name=item[(item.indexOf("]")+1)..item.indexOf(":")].to!string;
			
					chat.unread=item[(item.indexOf(":")+2)..(item.length-7)].to!int;
					dialogs~=chat;	
				}else{
					logInfo("Found bad item!!!! "~item);
				}
			}
		}
		catch(Exception e){
			logInfo("Exception: some dialog_list are broken "~e.msg);
			logInfo("Вот туточке:\n"~list.join("\n"));
		}

		reply.list=dialogs;
		return reply;
	}
	public Json getMessages(string userID){
		Json reply = Json.emptyObject;
		Json[] history;
		string[] list;

		try{
			string num;
			if(config.mod["messageCount"].type==Json.Type.string){
				num = config.mod["messageCount"].get!string;
			}else{
				num="50";
			}
			list=commSock("history "~userID~" "~num~"\n").splitLines;
		}catch(Exception e){
			logInfo("Ёбнулось при запросе истории! "~e.msg);
		}
		//logInfo("Parsing history...");
		string[] msgList;
		try{
			foreach(string item;list){
				if(item.indexOf(">>>")>0 ||item.indexOf("»»»")>0||item.indexOf("<<<")>0||item.indexOf("«««")>0){
					msgList~=item;
				}else{
					try{
						if(msgList.length>0){
							msgList[$-1]~="\n"~item;
						}else{
							logInfo("Trying to add to empty array: "~item);
						}
					}catch(Exception e){
						logInfo("Array has no elements! Trying to add: "~item);
						logInfo(e.msg);
					}
				}
			}
		}catch(Exception e){
			logInfo("Ёбнулось при чтении истории! "~e.msg);
			logInfo("Вот такие вот пироги:\n"~list.join("\n"));
		}
		foreach(string item;msgList){
			try{
				history~=parseMsg(item);
			}catch(Exception e){
				logInfo(e.msg);
				logInfo(item);
			}
		}
		reply.list=history;
		return reply;
	}

	public Json sendMessage(string peer,string message){
		Json reply = Json.emptyObject;
		string[dchar] tTable=[' ':"_"];
		string[dchar] tTable2=['\\':"\\\\",'\'':"\\\'",'\n':"\\n"];
		peer=translate(peer,tTable);
		message=translate(message,tTable2);
		//dumbSock("msg "~peer~" '"~message~"'\n");
		commSock("msg "~peer~" '"~message~"'\n");
		return reply;
	}
	public Json addContact(string phone, string firstName, string lastName){
		Json reply = Json.emptyObject;
		string[dchar] tTable=['\\':"\\\\",'\'':"\\\'",'\n':"\\n", ' ':"_", '\'':"\\'", '"':"\\\""];
		firstName=translate(firstName,tTable);
		lastName=translate(lastName,tTable);
		phone=translate(phone,tTable);
		string user=commSock("add_contact "~phone~" "~firstName~" "~lastName~"\n");
		try{
			user=user[1..user.indexOf("]")];
		}catch{

		}
		reply.user=user;
		return reply;
	}
	public Json delUser(string peer){
		string[dchar] tTable=[' ':"_"];
		peer=translate(peer,tTable);
		//write("del_contact "~peer~'\n');
		//dumbSock("del_contact "~peer~'\n');
		commSock("del_contact "~peer~'\n');
		return Json.emptyObject;
	}
	public Json getUserInfo(string peer){
		return Json.emptyObject;
	}
	public Json clearChat(string peer){
		return Json.emptyObject;
	}
	string getMime(string file){
		auto m = new Magic(MAGIC_MIME_TYPE);
		m.load(null);
		return m.file(file).dup;
	}
	private string downloadFile(string msg_id,string type){
		string fpath;
		fpath=commSock("load_"~type~" "~msg_id~'\n');
		try{
			fpath=fpath[9..(fpath.length)];
		}catch{
			logInfo("Oops! Error downloading file:"~msg_id~", fpath: "~fpath~"\n");
		}
		return fpath;
	}
	public string getBase64File(string msg_id,string type){
		string fpath=downloadFile(msg_id,type);
		logInfo("msg_id:"~msg_id~", fpath: "~fpath~"\n");
		if(fpath.length>0)
		{
			File f = File(fpath, "r");
			scope(exit) f.close();
			string file64;
			foreach(encoded; Base64.encoder(f.byChunk(3)))
			{
				file64~=encoded;
			}
			file64 = "data:"~getMime(fpath)~";base64,"~file64;
			return file64;
		}else{
			return "";
		}
	}
	public Variant[string] getFile(string msg_id,string type){
		string fpath=downloadFile(msg_id,type);
		Variant [string]r;
		if(fpath.length>0){
			r["ftype"]=getMime(fpath);
			r["file"]=cast(ubyte[])read(fpath);
		}
		return r;
	}
	this(Json c){
		config=Json.emptyObject;
		config=c;
		sockBlock = new TaskMutex;
	}
	~this(){
	}
	public void client(){
		try{
			if(m_sock is null){
				m_sock = new Socket(AddressFamily.UNIX,SocketType.STREAM);
				m_sock.connect(new UnixAddress("./assets/telegram/telegram.sock"));
				//m_sock.setOption(SocketOptionLevel.SOCKET, SocketOption.RCVTIMEO, dur!"msecs"(1500));
				m_sock.setOption(SocketOptionLevel.SOCKET, SocketOption.RCVTIMEO, dur!"msecs"(9000));
			}
		}
		catch(Exception e){
			logInfo("Exception: "~e.msg);
			m_sock=null;
			exit(0);
		}
	}
	private void dumbSock(string msg){
			if(m_sock !is null && m_sock.isAlive()){
				sockBlock.lock();
				m_sock.send(msg);
				sockBlock.unlock();	
			}else{
				logInfo("Telegram-cli is down!");
			}
	}
	private void clearSockBuffer(){
		char[1] buf;
		string junk;
		m_sock.setOption(SocketOptionLevel.SOCKET, SocketOption.RCVTIMEO, dur!"msecs"(1));
		while(m_sock.receive(buf)>0){
			junk~=buf;
		}
		if(junk.length>0)
			logInfo("Junk found: "~junk);
		//m_sock.setOption(SocketOptionLevel.SOCKET, SocketOption.RCVTIMEO, dur!"msecs"(1500));
		m_sock.setOption(SocketOptionLevel.SOCKET, SocketOption.RCVTIMEO, dur!"msecs"(9000));
	}
	private string commSock(string msg){
			//logInfo("Getting: "~msg.chomp());
			if(m_sock !is null && m_sock.isAlive()){
				char[7] ans;
				char[1] buf;
				string head;
				string output="";

				try{
					sockBlock.lock();
					clearSockBuffer();//case some old unswer is jammed in the buffer
					m_sock.send(msg);
					//auto sw = StopWatch(AutoStart.yes);
					long back = m_sock.receive(ans);
					//writefln("Waited %s ms until the socket timed out.", sw.peek.msecs);
					if(back>0){
						do{
							m_sock.receive(buf);
							head~=buf[0];
						}while(buf[0]!='\n');
						int bytes=head.stripRight().to!int;
						for(int i=0;i<bytes+1;i++){
							m_sock.receive(buf);
							output~=buf[0];
						}
						sockBlock.unlock();
						return output.stripRight();
					}else{
						//logInfo("No data =(");
						sockBlock.unlock();
						return "";
					}
				}
				catch(Exception e){
					logInfo("Exception: "~e.msg);
					sockBlock.unlock();
					exit(0);
				}
			}
			logInfo("Telegram-cli is down!");
			return "FAIL";
	}
}
