module tthread;

import std.stdio;
import std.string;
import std.process;
import core.sys.posix.signal: SIGKILL;
import core.thread;

import vibe.data.json;
import vibe.http.client;
import vibe.core.log;
import textops;

enum Escape{
	COLOR_RED = "\033[0;31m",
	COLOR_REDB ="\033[1;31m",
	COLOR_NORMAL = "\033[0m",
	COLOR_GREEN = "\033[32;1m",
	COLOR_GREY = "\033[37;1m",
	COLOR_YELLOW = "\033[33;1m",
	COLOR_BLUE = "\033[34;1m",
	COLOR_MAGENTA = "\033[35;1m",
	COLOR_CYAN = "\033[36;1m",
	COLOR_LCYAN = "\033[0;36m",

	COLOR_INVERSE = "\033[7m"
}

class TelegramThread : Thread
{
    this(int p)
    {
		port = p;
        super( &run );
    }
    ~this(){
    }
    public void stop(){
		//kill(proc.pid, SIGKILL);
    	logInfo("Trying to kill telegram");
    	try{
    		kill(proc.pid,SIGKILL);
    		wait(proc.pid);
    	}catch(Exception e){
    		writeln(e.msg);
    	}
    	logInfo("Telegram-cli terminated!");
    }
private :
	int port;
	ProcessPipes proc;

	void parseEvent(string event){
		string clean=stripControl(event);
		Json msg=Json.emptyObject;
		msg.data=Json.emptyObject;
		msg.data.params=parseMsg(clean);
		logInfo("#################TELEGRAM EVENT OUT");
		logInfo("B: "~msg["data"].toString());
		logInfo("################END TELEGRAM EVENT");
		msg.from="telegram";
		saveMessage(msg.data.params);
		loopFire("_PUSH_MESSAGE_", msg,"push");
	}
	void saveMessage(Json params){
		Json data = Json.emptyObject;
		data.data=Json.emptyObject;
		data.data["switch"]="SAVE_MESSAGE";
		data.data.params=params;
		data["to"]="pybot";
		data["from"]="telegram";
		loopFire("_DB_ACTION_",data, "save");
    }
	void loopFire(string event,Json data, string route){
		void fire(){
		//	try{
				requestHTTP("http://localhost:" ~port.to!string~ "/telegram/"~route,
					(scope req) {
						req.method = HTTPMethod.POST;
						req.writeJsonBody(["data":data]);
					},
					(scope res) {
					}
				);
		/*	}catch(Exception e){
				logInfo("Telegram - _PUSH_MESSAGE_");
				logInfo("PUSH failed: "~e.msg);
			}
		*/}
		new Thread(&fire).start();
	}
	void run()
	{
        logInfo("Derived Telegram thread running.");
        proc = pipeProcess(["./bin/telegram-cli","-RS","./assets/telegram/telegram.sock"], Redirect.stdout | Redirect.stderr);
		scope(exit){
			foreach (line; proc.stderr.byLine){
				logDebug("TelegramDBG stderr:"~line.idup);
			}
			stop();
		}

		string output;
		string input;
		bool multyline=false;
		foreach (line; proc.stdout.byLine){
			input = line.idup;
			logDebug("TelegramDBG stdout:"~input);
			if(input.indexOf("is typing")>0 || input.indexOf("updated photo")>0|| input.indexOf("was online")>0){
			//do something
			}else{
				if(multyline){
					output ~= input~'\n';
					if(input.lastIndexOf("[0m")==input.length-3){
						multyline=false;
						try{
							parseEvent(output);
						}catch(Exception e){
							logInfo("Ебнулось на парсинге мультилайна!"~ e.msg);
							logInfo(output);
						}
						output="";
					}
				}else{
					if(input.indexOf(Escape.COLOR_BLUE)>-1||input.indexOf(Escape.COLOR_GREEN)>-1){
						if(input.lastIndexOf("[0m")==input.length-3){
							output ~= input~'\n';
							try{
								parseEvent(output);
							}catch(Exception e){
								logInfo("Ебнулось на парсинге сингллайна!"~ e.msg);
								logInfo(output);
							}
							output="";						
						}else{
							multyline=true;
							output ~= input~'\n';
						}						
					}
				}
			}
		}
		
	}
}
