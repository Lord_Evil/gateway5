import vibe.d;
import vibe.data.json;
import vibe.utils.array;
import std.stdio;
import std.random;
import std.regex;
import std.datetime : Clock, stdTimeToUnixTime;
import std.string;
import std.math;
import std.uuid;

string[2] mod_name = ["_AUTH_","auth"];
string[] mod_hooks=["_PUSH_MESSAGE_","_AUTH_"];
string[] mod_routes=["","ws","login","logout","css/*","js/*","img/*","phone","who", "code"];
Json function(string,Json=Json.emptyObject) appFireEvent;

WebSocket[] m_socks;

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) int init() {
	logInfo(mod_name[1]~": Initializing!");
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) string[] routes() {
	return mod_routes;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) string[] hooks() {
	return mod_hooks;
}
void denyAccess(string userID, string name=""){
	if(name.length>0){
		sendMessage(userID,"Sorry, "~name~", you can not login!");
	}else{
		sendMessage(userID,"Sorry, you can not login!");
	}
}
void allowAccess(string userID, string name){
	sendMessage(userID,"Welcome, "~name~", you've been successfully logged in!");
}
void sendMessage(string userID, string message){
	Json data = Json.emptyObject;
	data.data = Json.emptyObject;
	data.data["switch"]="SEND_MESSAGE";
	data["to"]="telegram";
	data["from"]="auth";
	data.data.params = Json.emptyObject;
	data.data.params["userID"] = userID;
	data.data.params["message"] = message;
	appFireEvent("_SOCIAL_ACTION_",data);
}
string checkUser(string social, string socialID){
	//check whether user in DB, if not, add him.
	//check whether user has chatneyID, generate and add it.
	string chatneyID;
	Json query=Json.emptyObject;
	query.data=Json.emptyObject;
	query.data["switch"]="GET_PROFILE";
	query.data.params=Json.emptyObject;
	query.data.params["social"]=social;
	query.data.params["userid"]=socialID;
	query.from="auth";
	query["to"]="pybot";
	Json reply = Json.emptyObject;
	try{
		reply = appFireEvent("_DB_ACTION_",query);
	}catch{
		return "";
	}
	try{
		chatneyID = reply["chatneyID"].get!string;
	}catch{
		query.data=Json.emptyObject;
		query.data["switch"]="UPDATE_PROFILE";
		query.data.params=Json.emptyObject;
		query.data.params["social"]=social;
		query.data.params["userid"]=socialID;
		query.data.params["upkey"]="chatneyID";
		chatneyID = randomUUID().toString();
		query.data.params["upval"]=chatneyID;
		query.from="auth";
		query["to"]="pybot";
		try{
			reply = appFireEvent("_DB_ACTION_",query);
		}catch{
			return "";
		}
	}
	return chatneyID;
}
extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	if(event=="_AUTH_"){
		j.msg="/"~mod_name[1]~"/login";
	}
	if(event=="_PUSH_MESSAGE_"){
		string chatneyID;
		if(data["data"]["params"]["way"].get!string=="to") chatneyID=checkUser(data["from"].get!string,data["data"]["params"]["userID"].get!string);

		//if(data["data"]["params"]["way"].get!string=="to"&&match(data["data"]["params"]["data"].get!string,r"^\d\d\d\d\d\d$")){
		if(data["data"]["params"]["way"].get!string=="to"&&match(data["data"]["params"]["data"].get!string,r"^\d\d\d\d$")){
			//check if admin is loggin in trogh db
			string seq=data["data"]["params"]["data"].get!string;
			bool secret=false;
			if(data["data"]["params"]["secretID"].type==Json.Type.string){
				secret=true;
			}
			try{
				int code=seq.to!int;//get login code
				foreach(Login auth;awaiting){//see if any of the clients have this code
					if(auth.seq==code){
						if(!auth.external){
							Json query=Json.emptyObject;
							query.data=Json.emptyObject;
							query.data["switch"]="LIST_ADMIN";
							query.from="auth";
							query["to"]="pybot";
							Json reply = Json.emptyObject;
							try{
								reply = appFireEvent("_DB_ACTION_",query);
							}catch{
								return j;
							}
							bool noAdmin=true;
							foreach(Json admin; reply["docs"]){
								if(admin["id"].get!string==data["data"]["params"]["userID"].get!string){
									noAdmin = false;
									break;
								}
							}
							if(noAdmin){
								if(secret){
									denyAccess(data["data"]["params"]["secretID"].get!string,data["data"]["params"]["peer"].get!string);
								}else{
									denyAccess(data["data"]["params"]["userID"].get!string,data["data"]["params"]["peer"].get!string);
								}
								return j;
							}
							auth.status=true;
							auth.social=data["from"].get!string;
							auth.userid=data["data"]["params"]["userID"].get!string;
							auth.fullname=data["data"]["params"]["peer"].get!string;
							auth.role="admin";
							string key = auth.sockKey;
							foreach(WebSocket sock; m_socks){
								if(key==sock.request.headers["Sec-WebSocket-Key"]){
									Json resp=Json.emptyObject;
									resp.login="OK";
									if(secret){
										allowAccess(data["data"]["params"]["secretID"].get!string, auth.fullname);
									}else{
										allowAccess(auth.userid, auth.fullname);
									}
									sock.send(resp.toString());
									break;
								}
							}
							break;
						}else{
							/*
							logInfo("Let's login this user to external service: "~auth.callback);
							//PUSH CALLBACK WITH CODE AND USER ID(SOCIAL)
							try{
								requestHTTP(auth.callback,
									(scope req){
										req.method = HTTPMethod.POST;
										req.writeFormBody(["code":auth.seq.to!string,"social":data["from"].get!string,"socialID":data["data"]["params"]["userID"].get!string]);
									},
									(scope res){
										string message=res.read;
										if(secret){
											sendMessage(data["data"]["params"]["secretID"].get!string,message);
										}else{
											sendMessage(data["data"]["params"]["userID"].get!string,message);
										}
									}
								);
							}catch(Exception e){
								logInfo(e.msg);
							}
							*/
							auth.status=true;
							auth.social=data["from"].get!string;
							auth.userid=data["data"]["params"]["userID"].get!string;
							auth.chatneyID=chatneyID;
							break;
						}
					}
				}
			}catch(Exception e){
				logInfo(e.msg);
			}
			return j;
		}
	}
	return j;
}

bool checkSession(HTTPServerResponse res, HTTPServerRequest req)
{
		if(!config.global.login.get!bool) return true;//let them pass
		if (!req.session || !req.session.get!bool("auth")){ 
			return false;
		}else{
			return true;
		}
}

extern (C) void getRequest(HTTPServerResponse res, HTTPServerRequest req)
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	
	//logInfo(rpath);
	switch(rpath){
		case "/phone":
			res.writeJsonBody(["phone":config.mod["telegram_phone"].get!string]);
			break;
		case "/login":
				serveStaticFile("./assets/"~mod_name[1]~"/public_html/login.html")(req, res);
			break;
		case "/logout":
				if (!checkSession(res,req)) res.redirect("login");
				else
					res.terminateSession();
					res.writeBody("See you!");
			break;
		case "/who":
				if (!checkSession(res,req)) {
					res.writeJsonBody(["user":"NOT LOGGED IN"]);
				}else{
					Json j = Json.emptyObject;
					if(!config.global.login.get!bool)
						j.error="Login is not enabled!";
					else
						j.name=req.session.get!string("fullname");
					res.writeJsonBody(j);
				}
			break;
		case "/ws":
			handleWebSockets(&handleConn)(req,res);
			break;
		default:
			if(rpath.indexOf("/css/")>-1 ||rpath.indexOf("/js/")>-1 ||rpath.indexOf("/img/")>-1||rpath.indexOf("/files/")>-1||rpath.indexOf("/plugins/")>-1){
				serveStaticFile("./assets/"~mod_name[1]~"/public_html/"~rpath)(req, res);
			}else{
				res.redirect("/");
			}
			break;
	}
}
class Login{
	string sockKey;
	bool status=false;
	int seq;
	string social;
	string userid;
	string chatneyID;
	string fullname;
	string role;
	bool external = false;
	string session;
	long timer;
	this(string key, int s){
		sockKey=key;
		seq=s;
		timer=Clock.currTime().toUnixTime();
	}
	this(int s){
		seq=s;
		timer=Clock.currTime().toUnixTime();
	}
	~this(){}
}
Login awaiting[];

void handleConn(scope WebSocket sock)
{
	logInfo("Incomming connection! "~sock.request.clientAddress.to!string~" "~sock.request.headers["Sec-WebSocket-Key"]);
	//logInfo(sock.request.headers);
	m_socks~=sock;
	while (sock.waitForData()) {
		auto msg = sock.receiveText();
		if(msg=="login"){
			Json reply = Json.emptyObject;

			//int seq = uniform(100000,999999);//generate code
			int range=4;
			while(isFull(range)){
				range++;
			}

			int seq = uniform(1000,pow(10,range)-1);
			while(isUsedNumber(seq)){
				seq = uniform(1000,pow(10,range)-1);
			}

			Login auth = new Login(sock.request.headers["Sec-WebSocket-Key"],seq);
			awaiting~=auth;
			reply.seq=seq;
			sock.send(reply.toString());
			continue;
		}
	}
	logInfo("Connection closed! "~sock.request.headers["Sec-WebSocket-Key"]);

	m_socks.removeFromArray(sock);
	foreach(Login auth;awaiting){
		if(auth.sockKey == sock.request.headers["Sec-WebSocket-Key"]){
			if(auth.status==false)
				awaiting.removeFromArray(auth);
		}
	}
	sock=null;
}

bool isFull(int i){
	if(awaiting.length<pow(10,i)*0.65)
		return false;
	else
		return true;
}
bool isUsedNumber(int i){
	foreach(Login auth; awaiting){
		if(auth.seq==i) return true;
	}
	return false;
}
extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) //for some reason params come in reverse order when called from kernel
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	switch(rpath){
		case "/code":
			foreach(Login auth;awaiting){
				if(auth.session==req.form["session"]){
					if(auth.status){
						awaiting.removeFromArray(auth);
						res.writeJsonBody(["status":"login-ok","code":auth.seq.to!string,"social":auth.social,"socialID":auth.userid, "chatneyID":auth.chatneyID]);
						return;
					}else{
						res.writeJsonBody(["status":"awaiting","code":auth.seq.to!string]);
						return;
					}
				}
			}

			int range=4;
			while(isFull(range)){
				range++;
			}

			int seq = uniform(1000,pow(10,range)-1);
			while(isUsedNumber(seq)){
				seq = uniform(1000,pow(10,range)-1);
			}

			Login auth = new Login(seq);
			auth.external=true;
			auth.session=req.form["session"];
			awaiting~=auth;
			res.writeJsonBody(["status":"new","code":seq.to!string]);
			break;
		case "/login":
			foreach(Login auth;awaiting){
				try{
					if(auth.seq==req.form["seq"].to!int && auth.status){
						req.session = res.startSession();
						req.session.set("auth",true);
						req.session.set("active",Clock.currTime().toUnixTime());
						req.session.set("social",auth.social);
						req.session.set("userid",auth.userid);
						req.session.set("fullname",auth.fullname);
						req.session.set("role",auth.role);
						req.session.set("chatneyID",auth.chatneyID);
						awaiting.removeFromArray(auth);
						if(req.form["r"] != ""&&req.form["r"]!="/web/logout"&&req.form["r"]!="/auth/logout")
							res.redirect(req.form["r"]);
						else
							res.redirect("/");
					}
				}catch{}
			}
			break;
		default:break;
	}
}
shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
}
