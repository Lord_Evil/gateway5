import vibe.data.json;
import vibe.http.server;
import vibe.core.log;

import std.string;

string[2] mod_name = ["_SOCIAL_TEMPLATE_","social_template"];
string[] mod_hooks=["_SOCIAL_ACTION_"];
string[] mod_routes=["push","save","file/:msgID/:type/:fileName"];

extern (C) string[] routes() {
	return mod_routes;
}

Json function(string,Json=Json.emptyObject) appFireEvent;

Json config;
extern (C) void setConfig(Json c) {
	config = Json.emptyObject;
	config.global = c.global;
	config.mod = c.mod;
}

extern (C) int init() {
	logInfo(mod_name[1]~": Initializing!");
	return 1;
}

extern (C) string[2] name() {
	return mod_name;
}

extern (C) void setFE(int *appFEPtr) {
	appFireEvent=cast(Json function(string,Json))appFEPtr;
}

extern (C) string[] hooks() {
	return mod_hooks;
}

extern (C) Json raiseEvent(string event, Json data = Json.emptyObject) {
	Json j=Json.emptyObject;
	Json params=Json.emptyObject;
	if(data["data"]["params"].type==Json.Type.object){
		params=data["data"]["params"];
	}
	try{
		switch(data["data"]["switch"].get!string){
			case "IDENTIFY": //returns it's name
				j.data=Json.emptyObject;
				j.data.name=mod_name[1];//TODO: return module name, messanger type, client ID
				break;
			case "GET_CONTACTS": //returns list of contacts
				j.data=Json.emptyObject;
				break;
			case "GET_DIALOGS": //returns list of dialogs and unread messages
				j.data=Json.emptyObject;
				break;
			case "GET_MESSAGES": //returns last ten messages. TODO: add option to load dialog tops (more messages)
				j.data=getMessages(params["userID"].get!string);
				break;
			case "GET_LOCAL_MESSAGES":
				j.data=Json.emptyObject;
				break;
			case "SEND_MESSAGE": //sends message. TODO: images, files, documents, maps
				//sendMessage(params["userID"].get!string,params["message"].get!string);
				break;
			case "GET_FILE":
				//getFile(params["msgID"].get!string,params["type"].get!string); ?
				break;
			case "ADD_CONTACT":
				//addContact(params["userID"].get!string,params["fname"].get!string,params["lname"].get!string);
				break;
			//TODO: update contact, delete contact
			case "DELETE_CONTACT":
				//deleteContact(params["userID"].get!string);
				break;
			default:
				j.data="BAD_REQUEST";
				break;
		}
	}catch(Exception e){
		logInfo(mod_name[0]~": "~e.msg);
	}
	j.from=mod_name[1];
	j.res="OK";
	return j;
}

Json getMessages(string userid){
	Json data = Json.emptyObject;
	data.data=Json.emptyObject;
	data.data["switch"]="LIST_MESSAGES";
	data.data.params=Json.emptyObject;
	data.data.params["userID"]=userid;
	data["to"]="pybot";
	data["from"]=mod_name[1];
	return appFireEvent("_DB_ACTION_",data);
}

extern (C) void postRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath=="/push"){
		//res.headers["Access-Control-Allow-Origin"]="*";
		try{
			appFireEvent("_PUSH_MESSAGE_",req.json["data"]);
		}catch(Exception e){
			logInfo(e.msg);
		}
		
	}
	if(rpath=="/save"){
		//res.headers["Access-Control-Allow-Origin"]="*";
		try{
			appFireEvent("_DB_ACTION_",req.json["data"]);
		}catch(Exception e){
			logInfo(e.msg);
		}
	}
	res.writeBody("OK");
}

extern (C) void getRequest(HTTPServerResponse res, HTTPServerRequest req) 
{
	string rpath = req.path[req.path.indexOf("/",1)..$];
	if(rpath.indexOf("/file")>-1){ //getFile()
		try{

		}catch(Exception e){
			res.writeBody(e.msg);
		}
	}else{
		res.writeBody("OK");
	}
}
shared static this() {
//	logInfo(mod_name[0]~": I'm being loaded!");
}

shared static ~this() {
	logInfo(mod_name[0]~": I'm being unloaded!");
}
