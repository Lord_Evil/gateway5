import vibe.d;
import std.conv;
import std.file;
//import std.stdio;
import std.c.stdlib;

//Our modules
import kernel;
kernel kern;

alias void function(int) sighandler_t;
extern (C) sighandler_t signal(int signum, sighandler_t handler);
const int SIGHUP	= 1;	// Term Hangup detected on controlling terminal or death of controlling process
const int SIGINT	= 2;	// Interrupt from keyboard
const int SIGQUIT	= 3;	// Quit from keyboard
const int SIGILL	= 4;	// Illegal Instruction
const int SIGABRT	= 6;	// Abort signal from abort(3)
const int SIGFPE	= 8;	// Floating point exception
const int SIGKILL	= 9;	// Kill signal
const int SIGSEGV	= 11;	// Invalid memory reference
const int SIGPIPE	= 13;	// Broken pipe: write to pipe with no readers
const int SIGALRM	= 14;	// Timer signal from alarm(2)
const int SIGTERM	= 15;	// Termination signal
const int SIGUSR1	= 30;	// User-defined signal 1
const int SIGUSR1_2	= 10;	// User-defined signal 1
const int SIGUSR1_3	= 16;	// User-defined signal 1

Json config;

class appError : Throwable
{
    @safe pure nothrow this(string msg, Throwable next = null)
    {
        super(msg, next);
        bypassedException = null;
    }

    @safe pure nothrow this(string msg, string file, size_t line, Throwable next = null)
    {
        super(msg, file, line, next);
        bypassedException = null;
    }
    Throwable   bypassedException;
}
void sigusr1(int i){
	logDebug("What the fuck?"~i.to!string);
}
void shutDown(int i){
	logWarn("Signal caught! "~i.to!string~"\nShutting down!");
	kern.destroy();
	exitEventLoop();
}
shared static this()
{
	config = Json.emptyObject;
	Json configFile = Json.emptyObject;
	configFile = parseJsonString(readText("./config.json"));
	config.main = configFile.main;
	config.global = configFile.global;

	setLogFile("log/main.log",LogLevel.info);
	setLogFile("log/main_debug.log",LogLevel.debugV);
	//setLogFile("mainLog.log",cast(LogLevel)4);
	//auto logger = cast(shared)new HTMLLogger("log.html");
	//logger.lock().format = FileLogger.Format.threadTime;
	//registerLogger(logger);

	signal(SIGSEGV,function void(int i){throw new appError("SEGV");});
	signal(2,&shutDown);
	signal(15,&shutDown);
	signal(1,&sigusr1);
	//signal(SIGUSR1,&sigusr1);
	//signal(SIGUSR1_2,&sigusr1);
	//signal(SIGUSR1_3,&sigusr1);

	logInfo("Server started!");
	auto router = new URLRouter;
	router.get("/",&defaultRoute);
	kern = new kernel(&router, configFile);

	auto settings = new HTTPServerSettings;
	settings.port = config.global["port"].to!ushort;
	settings.sessionStore = new MemorySessionStore;
	settings.bindAddresses =  [config.global["host"].to!string];
	settings.errorPageHandler = toDelegate(&errorPage);
	settings.options=settings.options ^ HTTPServerOption.parseJsonBody;
	listenHTTP(settings, router);
	kern.startModules();
}
void defaultRoute(HTTPServerRequest req, HTTPServerResponse res){
	res.redirect(config.global["default_index"].get!string);
}
void errorPage(HTTPServerRequest req,
               HTTPServerResponse res,
               HTTPServerErrorInfo error)
{
	if(req.method==HTTPMethod.OPTIONS){
		res.headers["Access-Control-Allow-Origin"]="*";
		res.headers["Access-Control-Allow-Headers"]="content-type, accept";
		res.statusCode=200;
		res.writeBody("");
	}else{
//		writeln("CODE: "~error.code.to!string);
//		writeln("debug: "~error.debugMessage);
//		writeln("exception: "~error.exception.msg);
//		writeln("message: "~error.message);
		
		Json data=Json.emptyObject;
		data.from="main";
		data.message=error.message;
		data.request=req.toString;
		data.session=req.session.id;
		data.referer=req.headers.get("referer");
		data.res=cast(int)cast(int*)res;
		Json j = Json.emptyObject;
/*
		j = kern.fireEvent("_ERROR_",data);
		if(j["msg"].get!string!="undefined"){
			res.writeBody(j["msg"].get!string);
		}else{
			res.writeBody("404");
		}
*/
res.writeBody("404");
	}
	
}

shared static ~this(){
}
