module robocop;
import vibe.data.json;
import std.stdio;
import std.file;
import std.regex;

class Robocop{
private:
	string event;
	string to;
	string from;
	string _switch;
	string netmod_to;
	string netmod_from;
	string userID;
	string userRole;
	Json vars;
	Json inMessage;
	Json outMessage;
	Json config;

	Json filter(Json real_data, Json schema){
		Json obj = Json.emptyObject;
		Node rootNode = new Node(schema["rootNode"], "root", vars, real_data);
		obj=rootNode.getResult();
		return obj["root"];
	}
	class Node{
	private:
		string type;
		string condition;
		string name;
		Json data;//nodes(array), int, string etc. values
		Node parent;
		Json result;
		Json vars;
		Json real_data;
		Json rules;
		bool checkCondition(){
			writeln("Checking condition for "~(name.length>0?"node "~name:"array element")~" of type "~type);
			if(condition.length==1){
				switch(condition){
					case "*":
						return true;
					case "-":
						return false;
					default:
						return false;
				}
			}else{
				return chewExpression(condition);
			}
		}
		bool chewExpression(string condition){
			return eval(condition);
		}
		bool eval(string expr){
			string op;
			string left;
			string right;
			auto reg = ctRegex!(`(?P<left>\$[a-z]+|'.*?')(?P<op>=|!=)(?P<right>\$[a-z]+|'.*?')`);
			auto res = matchFirst(expr, reg);
			try{
				left=res["left"];
				op=res["op"];
				right=res["right"];
				//writeln("Condition was: "~left~op~right);
				left=checkVar(left);
				right=checkVar(right);
				//writeln("Condition is: "~left~op~right);
			}catch{
				writeln("Condition syntaxis error: "~condition);
			}
			if(op=="="){
				if(left==right){
					return true;
				}else{
					return false;
				}
			}
			if(op=="!="){
				if(left!=right){
					return true;
				}else{
					return false;
				}
			}
			return true;
		}
		string checkVar(string v){
			if(v[0]=='$'){
				return vars[v[1..$]].get!string;
			}else{
				return v[1..$-1];
			}
		}
	public:
		this(Json d, string n, Json v, Json rd){//schema data, node name, variables
			parent = null;
			type=d["type"].get!string;
			condition=d["condition"].get!string;
			name=n;
			real_data=rd;
			data = d["data"];
			vars = v;
			if(d["denyMessage"].type==Json.Type.string){
				vars["denyMessage"]=d["denyMessage"];
			}else{
				vars["denyMessage"]="Baker is busy today!";
			}
			if(d["rules"].type==Json.Type.object){
				rules=d["rules"];
			}else{
				rules=Json.emptyObject;
				rules.access="allow";
			}
			//writeln("Our rules are: "~getRules.toString);
			runNode();
		}
		this(Node p, Json d, string n, Json v, Json rd
		){//parent node, schena data, node name, variables
			parent = p;
			vars = v;
			real_data=rd;
			type=d["type"].get!string;
			condition=d["condition"].get!string;
			name=n;
			data = d["data"];
			if(d["rules"].type==Json.Type.object){
				rules=d["rules"];
			}else{
				rules=parent.getRules();
			}
			runNode();
		}
		Json getRules(){
			return rules;
		}
		Json getResult(){
			return result;
		}
		string getName(){
			return name.dup;
		}
		private void runNode(){
			result = Json.emptyObject;
			if(checkCondition()){
				switch(type){
					case "object":
						if(name.length>0){
							result[name]=Json.emptyObject;
						}else{
							result=Json.emptyObject;
						}
						foreach(string key, Json node; data){
							//writeln("Initializing "~key);
							Node child = new Node(this, node, key, vars, real_data[key]);
							if(name.length>0){
								result[name]=child.getResult();
							}else{
								result=child.getResult();
							}
						}
						break;
					case "array":
						result[name]=Json.emptyArray;
						foreach(Json realNode; real_data){
							Node child = new Node(this, data, "", vars, realNode);
							result[name]~=child.getResult();
						}
						break;
					case "int":
						if(rules["access"]=="allow"){//if allow-deny
							if(rules["deny"].type==Json.Type.object && rules["deny"]["roles"].type==Json.Type.array){
								if(rules["deny"]["roles"][vars["userRole"].get!string].type==Json.Type.int_){
									result[name] = 0;
								}else{
									result[name] = real_data;
								}
							}else{
								result[name] = real_data;
							}
						}else{//if deny-allow
							if(rules["allow"].type==Json.Type.object && rules["allow"]["roles"].type==Json.Type.object){
								if(rules["allow"]["roles"][vars["userRole"].get!string].type==Json.Type.int_){
									result[name] = real_data;
								}else{
									result[name] = 0;
								}
							}else{
								result[name] = 0;
							}
						}
						break;
					case "string":
						if(rules["access"]=="allow"){//if allow-deny
							if(rules["deny"].type==Json.Type.object && rules["deny"]["roles"].type==Json.Type.array){
								if(rules["deny"]["roles"][vars["userRole"].get!string].type==Json.Type.int_){
									result[name] = "No cake for "~vars["userID"].get!string~" with role "~vars["userRole"].get!string;
								}else{
									result[name] = real_data;
								}
							}else{
								result[name] = real_data;
							}
						}else{//if deny-allow
							if(rules["allow"].type==Json.Type.object && rules["allow"]["roles"].type==Json.Type.object){
								if(rules["allow"]["roles"][vars["userRole"].get!string].type==Json.Type.int_){
									result[name] = real_data;
								}else{
									//result[name] = "No cake for "~vars["userID"].get!string~" with role "~vars["userRole"].get!string;
									result[name] = vars["denyMessage"];
								}
							}else{
								//result[name] = "No cake for "~vars["userID"].get!string~" with role "~vars["userRole"].get!string;
								result[name] = vars["denyMessage"];
							}
						}
						break;
					case "endobject":
						result[name] = real_data;
						break;
					default:break;
				}

			}else{
				if(parent.type!="array")
					result[name]="Conditions are not met";
				else
					result="Conditions are not met";
			}

		}
	}
public:
	this(string e, Json message, Json c){
		config=c;
		vars=Json.emptyObject;
		event=e;
		vars["event"]=e;
		from=message["from"].get!string;
		vars["from"]=from;
		//writeln(message.toString);
		if(from=="netmod" && message["data"]["netmod_from"].type()!=Json.Type.undefined){
			netmod_from=message["netmod_from"].get!string;
			vars["netmod_from"]=netmod_from;
		}
		if(message["data"].type!=Json.Type.undefined){
			if(message["data"]["switch"].type!=Json.Type.undefined){
				_switch=message["data"]["switch"].get!string;
				vars["switch"]=_switch;
			}
			vars["inMessage"]=message["data"];
		}
		if(message["to"].type()!=Json.Type.undefined)
		{
			to=message["to"].get!string;
			vars["to"]=to;
			if(to=="netmod" && message["netmod_to"].type()!=Json.Type.undefined){
				netmod_to=message["netmod_to"].get!string;
				vars["netmod_to"]=netmod_to;
			}
		}
		
		if(from=="api" && config.global.login.get!bool){
			userID=message["data"]["user"]["userID"].to!string;
			userRole=message["data"]["user"]["userRole"].get!string;
			vars["userID"]=userID;
			vars["userRole"]=userRole;
		}
		inMessage=message;
	}
	Json checkIn(string t){
		if(vars["to"].type()==Json.Type.undefined) vars["to"]=t;
		Json obj = Json.emptyObject;
		obj=inMessage;
		return obj;
	}
	Json checkOut(Json message, string f, string t){
		Json obj = Json.emptyObject;
		vars["from"]=f;
		vars["to"]=t;
		outMessage=message;
		vars["message"]=message;
		Json schema=Json.emptyObject;
				try{
					schema = parseJsonString(readText("./robocop/"~vars["from"].get!string~".json"));
					if(schema["switch"]==_switch){
						obj=filter(message,schema);
					}else{
						obj=message;
					}
				}catch{
					return message;
				}
		return obj;
	}
}
