module kernel;
import vibe.d;
import vibe.utils.array;
import std.conv;
import std.stdio;
import std.file;
import core.sys.posix.dlfcn;
import core.sync.mutex;
import std.algorithm: SwapStrategy;
import robocop;

string mod_dir="modules/";
kernel m_kern;

Json appFireEvent(string event, Json data=Json.emptyObject){
	return m_kern.fireEvent(event,data);
}
void dummyRequest(HTTPServerRequest req, HTTPServerResponse res){};
Json dummyRaiseEvent(string event, Json data){
	return Json.emptyObject;
}
string[] dummyHooks(){
	string[] s;
	return s;
}
class kernel{
	private URLRouter *k_router;
	private TaskMutex fireBlock;
	private Json config;
	private class Module{
		public void* handler;
		public string[2] name;
		public string[] function() hooks;
		public string[] routes;
		public void function(HTTPServerRequest, HTTPServerResponse) getRequest;
		public void function(HTTPServerRequest, HTTPServerResponse) postRequest;
		public Json function(string, Json) raiseEvent;
		public int function() start;

		public void init(){
			char *error = dlerror();
			
			if(dlsym(handler, "init")!=null){
				start = cast(int function())dlsym(handler, "init");
		  		if (error) {
    				logError("dlsym error - init: "~ to!string(error)~"\n handler: "~handler.to!string);
    				modules.removeFromArray(this);
    				return;
  				}
			}else{
				dlerror();
				modules.removeFromArray(this);
    			return;
			}

			if(dlsym(handler, "name")!=null){
  				string[2] function() getName = cast(string[2] function())dlsym(handler, "name");
	  			if (error) {
	    			logError("dlsym error - name: "~ to!string(error));
	    			modules.removeFromArray(this);
    				return;
  				}else{
					name=getName();
	  				logInfo("Module name: "~name[0]~":"~name[1]);
  				}
  			}else{
  				dlerror();
  				modules.removeFromArray(this);
    			return;
  			}
  			
  			if(dlsym(handler, "hooks")!=null){
  				hooks = cast(string[] function())dlsym(handler, "hooks");
		  		if (error) {
    					logError("dlsym error - hooks: "~ to!string(error));
  				}else{
					string[] m_hooks = hooks();
  					logInfo("Exports %d hooks",m_hooks.length);
  					foreach(string hook;m_hooks){
		  				logInfo("\t"~hook);
  					}
  				}
  			}else{
  				dlerror();
				hooks = cast(string[] function())&dummyHooks;
  			}
			if(dlsym(handler, "setConfig")!=null){
				void function(Json) setConfig = cast(void function(Json))dlsym(handler, "setConfig");
				if (error) {
					logError("dlsym error - setConfig: "~ to!string(error));
				}else{
					Json c = Json.emptyObject;
					c.global=config["global"];
					c.mod=config["modules"][name[1]];
					setConfig(c);
				}
			}else{
				 dlerror();
			}

			if(dlsym(handler, "raiseEvent")!=null){
				raiseEvent = cast(Json function(string, Json))dlsym(handler, "raiseEvent");
				if (error) {
					logError("dlsym error: "~ to!string(error));
				}
  			}else{
  				dlerror();
  				raiseEvent=cast(Json function(string, Json))&dummyRaiseEvent;
  			}
  			
  			
  			if(dlsym(handler, "setFE")!=null){
  				void function(int*) setFE = cast(void function(int*))dlsym(handler, "setFE");
  				if (error) {
	    			logError("dlsym error - setFE: "~ to!string(error));
  				}else{
  					setFE(cast(int*)&appFireEvent); //set pointer to appFireEvent()
  				}
  			}else{
  				dlerror();
  			}

			
  			if(dlsym(handler, "routes") is null){
  				dlerror();
			}else{
  				string[] function() getRoutes = cast(string[] function())dlsym(handler, "routes");
	  			if (error) {
	    			logError("dlsym error - routes: "~ to!string(error));
  				}else{
  					routes=getRoutes();
					if(routes.length>0){addRoutes(routes,name[1]);}

					if(dlsym(handler, "getRequest") is null){
						dlerror();
						getRequest = cast(void function(HTTPServerRequest, HTTPServerResponse))&dummyRequest;
					}
					else{
						getRequest = cast(void function(HTTPServerRequest, HTTPServerResponse))dlsym(handler, "getRequest");
						if (error) {
	    					logError("dlsym error - getRequest: "~ to!string(error));
  						}
					}
					if(dlsym(handler, "postRequest") is null){
						dlerror();
						postRequest = cast(void function(HTTPServerRequest, HTTPServerResponse))&dummyRequest;
					}
					else{
						postRequest = cast(void function(HTTPServerRequest, HTTPServerResponse))dlsym(handler, "postRequest");
	  					if (error) {
	    					logError("dlsym error - postRequest: "~ to!string(error));
  						}
					}
  				}
			}
		}
	}
	private Module[] modules;
	private void loadModules(){
		void *lh;
		foreach(string path; sort!("a.name < b.name",SwapStrategy.stable)(dirEntries(mod_dir,"*.so" ,SpanMode.shallow).array)){
			lh = dlopen(cast(char*)path.dup, RTLD_LAZY);
			if (!lh) {
				logError("dlopen error: "~ to!string(dlerror()));
			}else{
				auto mod=new Module;
				mod.handler=lh;
				modules~=mod;
			}
		}
	}
	private void queryModules(){
		logInfo("Initializing modules");
		foreach(Module mod; modules){
			mod.init();
		}
		logInfo("All modules initialized");
	}
	//TODO: modules could be started as threads
	public void startModules(){
		logInfo("Starting modules");
		foreach(Module mod; modules){
			mod.start();
		}
	}
	private void addRoutes(string[] routes, string name){
		foreach(string route;routes){
			route="/"~name~"/"~route;
			logInfo("Adding route for module "~name~" : "~route);
			k_router.any(route,&takeRoute);
		}
	}
	private void takeRoute(HTTPServerRequest req, HTTPServerResponse res){
		string name=req.path[1..req.path.indexOf("/",1)];
		foreach(Module mod; modules){
			if(name==mod.name[1]){
					if(req.method==HTTPMethod.POST){
						if(req.contentType=="application/json" && mod.name[1]!="api"){
							auto bodyStr = cast(string)req.bodyReader.readAllUTF8();
							if (!bodyStr.empty) req.json = parseJson(bodyStr);
						}
						mod.postRequest(req,res);
					}else{
						mod.getRequest(req,res);
					}
			}
		}
	}

	public Json fireEvent(string event, Json data = Json.emptyObject){
		static int eventCounter=0;
		eventCounter++;
		int m_counter=eventCounter;
		logInfo("#####################EVENT CALL #"~m_counter.to!string);
		logInfo(m_counter.to!string~"#Event fired: "~event);
		logInfo(m_counter.to!string~"#Event from: "~data["from"].toString());
		logInfo(m_counter.to!string~"#Event data: "~data.toString());
		if(data["from"].to!string=="undefined"){
			logInfo(m_counter.to!string~"#Event sender undefined!");
			return Json.emptyObject;
		}
		Json j = Json.emptyObject;
		if(data["to"].to!string=="undefined") j.list = Json.emptyArray;

		Robocop murphy = new Robocop(event, data, config);

		foreach(Module mod; modules){
			string[] m_hooks = mod.hooks();
			foreach(string hook;m_hooks){
				if(event==hook){
					//logInfo(data["to"].to!string~":"~mod.name[1]);
					if(data["to"].to!string!="undefined"){
						if(mod.name[1]==data["to"].to!string){
							logInfo(m_counter.to!string~"#Sending event to: "~mod.name[1]);
							data = murphy.checkIn(mod.name[1]);
							//j=mod.raiseEvent(event,data);
							j = murphy.checkOut(mod.raiseEvent(event,data), mod.name[1], data["from"].to!string);
							logInfo(m_counter.to!string~"#Back from: "~mod.name[1]);
						}
					}else{
						logInfo(m_counter.to!string~"#Sending event to: "~mod.name[1]);
						data = murphy.checkIn(mod.name[1]);
						//j.list~=mod.raiseEvent(event,data);
						j.list~=murphy.checkOut(mod.raiseEvent(event,data), mod.name[1], data["from"].to!string);
						logInfo(m_counter.to!string~"#Back from: "~mod.name[1]);
					}
				}
			}
		}
		logInfo("#################END EVENT CALL #"~m_counter.to!string);
		return j;
	}

	this(URLRouter *router, Json c){
		config=c;
		k_router=router;
		m_kern=this;
//		fireBlock = new TaskMutex;
//		fireBlock.lock();
		logInfo("Kernel initialized!");
		logInfo("Loading modules...");
		loadModules();
		queryModules();
		logInfo("%d modules are loaded",modules.length);
//		fireBlock.unlock();
	}

	~this(){
		logInfo("Kernel is shutting down!");
		logInfo("Unloading modules...");
		foreach(Module mod; modules){
			void *hl = mod.handler;
			string name = mod.name[1].dup;
			try{
				mod.destroy();
				dlclose(hl);
			}catch(Exception e){
				logError("Unload failed! "~e.msg);
			}
			logInfo(name~" unloaded successfully");
		}
		logInfo("All modules unloaded!");

	}
}
